

## Referencias

[Fuentes del producto Github](https://github.com/NiceCircuits/modules_from_china.git)

[Driver windows para ch340](https://www.arduined.eu/ch340g-converter-windows-7-driver-download/)

[Otras referencias sobre el producto](http://nicecircuits.com/ch340g-usb-to-rs232-ttl-module-schematic-d-sun-v3-0/)

[Esquematico esp8266 realizado con ch340g](https://github.com/nodemcu/nodemcu-devkit)

## Tienda

[electronicaplugandplay $3000](https://www.electronicaplugandplay.com/circuitos-integrados/product/551-transceptor-usb-a-rs-232-ch340?search=ch340)

[bigtronica $5000](https://www.bigtronica.com/poblado/rs232/rs485/217-conversor-usb-serial-ch340.html)


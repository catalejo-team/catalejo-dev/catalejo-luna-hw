# TP5400

Vin = 5V
Vout = 5V
Iout = 1A
Batería Lipo 3.7V

Cargador de powerbank


## datasheet

[Traducción de datasheet al ingles](https://wnsnty.xyz/entry/tp5400-datasheet-translated)

## Blogs de referencia

[Tiny USB Batteries 3v](http://wtarreau.blogspot.com/2017/12/tiny-usb-batteries-v3.html)


## Símbolos

[EasyEda](https://easyeda.com/components/TP5400-1_317b70961db94764b06c67fd3ece0664)

## Tiendas

[Az Delivery tienda alemana](https://www.az-delivery.de/es/products/tp5400-usb-powerbank-modul)

[Ferretronica bogotá](https://ferretronica.com/products/modulo-carga-y-descarga-baterias-litio-tp5400-5v-1-2a)


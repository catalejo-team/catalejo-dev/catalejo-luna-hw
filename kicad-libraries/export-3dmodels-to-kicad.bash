KICAD_VERSION=7.0
rm -rf ~/.local/share/kicad/$KICAD_VERSION/3dmodels/*
ln -sr ./grabcad ~/.local/share/kicad/$KICAD_VERSION/3dmodels
ln -sr ./my-components ~/.local/share/kicad/$KICAD_VERSION/3dmodels
ln -sr ./power ~/.local/share/kicad/$KICAD_VERSION/3dmodels
ln -sr ./KiCad/ ~/.local/share/kicad/$KICAD_VERSION/3dmodels
ln -sr ./componentsearchengine/ ~/.local/share/kicad/$KICAD_VERSION/3dmodels


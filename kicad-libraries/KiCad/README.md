# Librerías

* [3D Model Library - Connector_PinHeader_2.54mm](https://kicad.github.io/download/packages3d/Connector_PinHeader_2.54mm.3dshapes.7z)

```bash
wget https://kicad.github.io/download/packages3d/Connector_PinHeader_2.54mm.3dshapes.7z
7z x Connector_PinHeader_2.54mm.3dshapes.7z
```

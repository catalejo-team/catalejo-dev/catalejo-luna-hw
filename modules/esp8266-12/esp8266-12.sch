EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 8900 3450 2    50   ~ 0
0
Text Label 8900 3600 2    50   ~ 0
1
Text Label 8900 3750 2    50   ~ 0
2
Text Label 8900 3900 2    50   ~ 0
3
Text Label 8900 4050 2    50   ~ 0
4
Text Label 8900 4200 2    50   ~ 0
5
Text Label 8900 4350 2    50   ~ 0
6
Text Label 8900 4500 2    50   ~ 0
7
Text Label 8900 4650 2    50   ~ 0
8
Text Label 8900 4800 2    50   ~ 0
9
Text Label 8900 4950 2    50   ~ 0
10
Text Notes 8600 3100 0    50   ~ 0
Pin -> Observación\n0 [16] -> compartido, wake up, no pwm, gpio\n1 [14] -> libre, pwm, gpio\n2 [12] -> libre, pwm, gpio\n3 [RX] -> compartido, RX, pwm, gpio\n4 [TX] -> compartido, TX, pwm, gpio\n5 [2] -> compartido: WS2812B, pull up, pwm\n6 [13] -> libre, pwm, gpio\n7 [15] -> exclusivo: PARLANTE, pull down para iniciar\n8 [0] -> exclusivo: BTN USER, pull up para iniciar\n9 [5] -> exclusivo: SDA por SW\n10[4]-> exclusivo: SCL por SW, sirve para pwm, gpio
Text HLabel 9000 3300 2    50   Input ~ 0
A0
Text Label 8900 3300 2    50   ~ 0
ADC_EXT
Wire Wire Line
	9000 3300 8900 3300
Text HLabel 9000 3450 2    50   BiDi ~ 0
16
Text HLabel 9000 3600 2    50   BiDi ~ 0
14
Text HLabel 9000 3750 2    50   BiDi ~ 0
12
Text HLabel 9000 3900 2    50   BiDi ~ 0
RX
Text HLabel 9000 4050 2    50   BiDi ~ 0
TX
Text HLabel 9000 4200 2    50   BiDi ~ 0
2
Text HLabel 9000 4350 2    50   BiDi ~ 0
13
Text HLabel 9000 4500 2    50   BiDi ~ 0
15
Text HLabel 9000 4650 2    50   BiDi ~ 0
0
Text HLabel 9000 4800 2    50   BiDi ~ 0
5
Text HLabel 9000 4950 2    50   BiDi ~ 0
4
Wire Wire Line
	9000 3450 8900 3450
Wire Wire Line
	9000 3600 8900 3600
Wire Wire Line
	9000 3750 8900 3750
Wire Wire Line
	9000 3900 8900 3900
Wire Wire Line
	9000 4050 8900 4050
Wire Wire Line
	9000 4200 8900 4200
Wire Wire Line
	9000 4350 8900 4350
Wire Wire Line
	9000 4500 8900 4500
Wire Wire Line
	9000 4650 8900 4650
Wire Wire Line
	9000 4800 8900 4800
Wire Wire Line
	9000 4950 8900 4950
Wire Wire Line
	7550 3250 7550 3350
Text Label 7550 3250 1    50   ~ 0
ADC_EXT
Wire Wire Line
	7550 4150 7550 4250
Text Label 7550 4250 3    50   ~ 0
GND
Text Notes 7650 3800 0    50   ~ 0
1% de\ntolerancia
Text Label 3100 4750 3    50   ~ 0
GND
Connection ~ 7550 3750
Wire Wire Line
	7550 3850 7550 3750
Wire Wire Line
	7550 3750 7450 3750
Wire Wire Line
	7550 3650 7550 3750
Text Label 7450 3750 2    50   ~ 0
ADC
$Comp
L Device:R R14
U 1 1 5EB453ED
P 7550 4000
F 0 "R14" H 7620 4046 50  0000 L CNN
F 1 "100K" H 7620 3955 50  0000 L CNN
F 2 "" V 7480 4000 50  0001 C CNN
F 3 "~" H 7550 4000 50  0001 C CNN
	1    7550 4000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R13
U 1 1 5EB45036
P 7550 3500
F 0 "R13" H 7620 3546 50  0000 L CNN
F 1 "220K" H 7620 3455 50  0000 L CNN
F 2 "" V 7480 3500 50  0001 C CNN
F 3 "~" H 7550 3500 50  0001 C CNN
	1    7550 3500
	1    0    0    -1  
$EndComp
Connection ~ 5450 4650
Wire Wire Line
	5450 4650 6350 4650
Connection ~ 5800 2350
Wire Wire Line
	5800 2350 6350 2350
Wire Wire Line
	5800 2350 5800 2550
Wire Wire Line
	5450 3400 6350 3400
$Comp
L Device:R R1
U 1 1 5EF19681
P 6350 3150
F 0 "R1" H 6420 3196 50  0000 L CNN
F 1 "12k" H 6420 3105 50  0000 L CNN
F 2 "" V 6280 3150 50  0001 C CNN
F 3 "~" H 6350 3150 50  0001 C CNN
	1    6350 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5EF1968B
P 6350 3650
F 0 "C2" H 6465 3696 50  0000 L CNN
F 1 "100n" H 6465 3605 50  0000 L CNN
F 2 "" H 6388 3500 50  0001 C CNN
F 3 "~" H 6350 3650 50  0001 C CNN
	1    6350 3650
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW2
U 1 1 5EF19695
P 6950 3700
F 0 "SW2" V 6904 3652 50  0000 R CNN
F 1 "flash" V 6995 3652 50  0000 R CNN
F 2 "" H 6950 3900 50  0001 C CNN
F 3 "~" H 6950 3900 50  0001 C CNN
	1    6950 3700
	0    1    1    0   
$EndComp
Text Notes 6800 3350 0    50   ~ 0
botón de\nusuario
Wire Wire Line
	6950 3500 6950 3400
Wire Wire Line
	6950 3400 6350 3400
Wire Wire Line
	6350 3300 6350 3400
Wire Wire Line
	6350 3400 6350 3500
Connection ~ 6350 3400
Wire Wire Line
	6950 3900 6950 4000
Wire Wire Line
	6950 4000 6350 4000
Wire Wire Line
	6350 4000 6350 3800
Wire Wire Line
	6350 3000 6350 2350
Wire Wire Line
	6350 4650 6350 4000
Connection ~ 6350 4000
Connection ~ 3300 2350
$Comp
L esp32-8266:ESP-12E U1
U 1 1 5EF05391
P 4550 3300
F 0 "U1" H 4550 4065 50  0000 C CNN
F 1 "ESP-12E" H 4550 3974 50  0000 C CNN
F 2 "" H 4550 3300 50  0001 C CNN
F 3 "http://l0l.org.uk/2014/12/esp8266-modules-hardware-guide-gotta-catch-em-all/" H 4550 3300 50  0001 C CNN
	1    4550 3300
	1    0    0    -1  
$EndComp
NoConn ~ 4300 4200
NoConn ~ 4400 4200
NoConn ~ 4500 4200
NoConn ~ 4600 4200
NoConn ~ 4700 4200
NoConn ~ 4800 4200
$Comp
L Device:R R3
U 1 1 5EF23C1C
P 3300 2750
F 0 "R3" H 3370 2796 50  0000 L CNN
F 1 "12k" H 3370 2705 50  0000 L CNN
F 2 "" V 3230 2750 50  0001 C CNN
F 3 "~" H 3300 2750 50  0001 C CNN
	1    3300 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 3200 3650 3200
$Comp
L Device:R R2
U 1 1 5EF25682
P 5800 2700
F 0 "R2" H 5870 2746 50  0000 L CNN
F 1 "12k" H 5870 2655 50  0000 L CNN
F 2 "" V 5730 2700 50  0001 C CNN
F 3 "~" H 5800 2700 50  0001 C CNN
	1    5800 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 3500 5800 3500
Wire Wire Line
	5800 3500 5800 2850
$Comp
L Device:R R5
U 1 1 5EF26234
P 5800 3850
F 0 "R5" H 5870 3896 50  0000 L CNN
F 1 "12k" H 5870 3805 50  0000 L CNN
F 2 "" V 5730 3850 50  0001 C CNN
F 3 "~" H 5800 3850 50  0001 C CNN
	1    5800 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 3600 5800 3600
Wire Wire Line
	5800 3600 5800 3700
Wire Wire Line
	5800 4000 5800 4100
Wire Wire Line
	5800 4100 5450 4100
Wire Wire Line
	5450 4100 5450 3700
Wire Wire Line
	3300 3200 3300 2900
Wire Wire Line
	3100 2350 3300 2350
$Comp
L Device:C C?
U 1 1 5EF30BE1
P 3100 4150
F 0 "C?" H 3215 4196 50  0000 L CNN
F 1 "100p" H 3215 4105 50  0000 L CNN
F 2 "" H 3138 4000 50  0001 C CNN
F 3 "~" H 3100 4150 50  0001 C CNN
	1    3100 4150
	1    0    0    -1  
$EndComp
Text Notes 3400 4150 0    50   ~ 0
Bypass
Text Label 3550 3100 2    50   ~ 0
ADC
Wire Wire Line
	3650 3100 3550 3100
Wire Wire Line
	5450 4100 5450 4650
Connection ~ 5450 4100
Text Label 3550 3300 2    50   ~ 0
0
Text Label 3550 3400 2    50   ~ 0
1
Wire Wire Line
	3550 3400 3650 3400
Text Label 3550 3500 2    50   ~ 0
2
Wire Wire Line
	3650 3500 3550 3500
Text Label 3550 3600 2    50   ~ 0
6
Wire Wire Line
	3650 3600 3550 3600
Text Label 5550 3000 0    50   ~ 0
4
Wire Wire Line
	5550 3000 5450 3000
Text Label 5550 3100 0    50   ~ 0
3
Wire Wire Line
	5550 3100 5450 3100
Text Label 5550 3200 0    50   ~ 0
9
Wire Wire Line
	5550 3200 5450 3200
Text Label 5550 3300 0    50   ~ 0
10
Wire Wire Line
	5550 3300 5450 3300
Text Label 5550 3400 0    50   ~ 0
8
Text Label 5550 3500 0    50   ~ 0
5
Text Label 5550 3600 0    50   ~ 0
7
Wire Wire Line
	3300 2600 3300 2350
$Comp
L Device:R R11
U 1 1 5EB347D6
P 2850 3300
F 0 "R11" V 2643 3300 50  0000 C CNN
F 1 "470" V 2734 3300 50  0000 C CNN
F 2 "" V 2780 3300 50  0001 C CNN
F 3 "~" H 2850 3300 50  0001 C CNN
	1    2850 3300
	0    1    1    0   
$EndComp
Wire Wire Line
	1900 3300 1900 3000
Wire Wire Line
	2000 3300 1900 3300
Text Notes 1950 4100 0    50   ~ 0
Habilitador del\nmodo de dormir\npor software.\n\nÉste elemento se\npuede quitar sino\nse hace uso de gpio16\ncomo un gpio
Connection ~ 1900 3000
Text HLabel 750  2250 1    50   Input ~ 0
+3.3V
$Comp
L Device:R R4
U 1 1 5EF0B4E4
P 1500 2750
F 0 "R4" H 1570 2796 50  0000 L CNN
F 1 "12k" H 1570 2705 50  0000 L CNN
F 2 "" V 1430 2750 50  0001 C CNN
F 3 "~" H 1500 2750 50  0001 C CNN
	1    1500 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5EF0B7F5
P 1500 3250
F 0 "C1" H 1615 3296 50  0000 L CNN
F 1 "100n" H 1615 3205 50  0000 L CNN
F 2 "" H 1538 3100 50  0001 C CNN
F 3 "~" H 1500 3250 50  0001 C CNN
	1    1500 3250
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW1
U 1 1 5EF0BF6C
P 1100 3300
F 0 "SW1" V 1054 3252 50  0000 R CNN
F 1 "reset" V 1145 3252 50  0000 R CNN
F 2 "" H 1100 3500 50  0001 C CNN
F 3 "~" H 1100 3500 50  0001 C CNN
	1    1100 3300
	0    -1   1    0   
$EndComp
Wire Wire Line
	1500 2900 1500 3000
Wire Wire Line
	1100 3100 1100 3000
Wire Wire Line
	1100 3000 1500 3000
Connection ~ 1500 3000
Wire Wire Line
	1500 3000 1500 3100
Wire Wire Line
	1100 3500 1100 3600
Wire Wire Line
	1100 3600 1500 3600
Wire Wire Line
	1500 3600 1500 3400
Wire Wire Line
	1900 3000 1500 3000
Wire Wire Line
	1500 2600 1500 2350
Wire Wire Line
	750  2350 1500 2350
Text HLabel 750  4750 3    50   Input ~ 0
GND
Wire Wire Line
	750  4650 1500 4650
Wire Wire Line
	750  4750 750  4650
Wire Wire Line
	750  2250 750  2350
Wire Wire Line
	1500 3600 1500 4650
Connection ~ 1500 3600
Connection ~ 1500 4650
Wire Wire Line
	1900 3000 3650 3000
Wire Wire Line
	1500 4650 3100 4650
Wire Wire Line
	3000 3300 3650 3300
Wire Wire Line
	2600 3300 2700 3300
Wire Wire Line
	3300 2350 5800 2350
Wire Wire Line
	3100 3700 3100 2350
Wire Wire Line
	1500 2350 3100 2350
Connection ~ 1500 2350
Connection ~ 3100 2350
$Comp
L Switch:SW_DIP_x01 SW3
U 1 1 5EF071F6
P 2300 3300
F 0 "SW3" H 2300 3567 50  0000 C CNN
F 1 "SW_DIP_x01" H 2300 3476 50  0000 C CNN
F 2 "" H 2300 3300 50  0001 C CNN
F 3 "~" H 2300 3300 50  0001 C CNN
	1    2300 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 3700 3650 3700
Wire Wire Line
	3100 4000 3100 3700
Connection ~ 3100 3700
Wire Wire Line
	3100 4300 3100 4650
Connection ~ 3100 4650
Wire Wire Line
	3100 4750 3100 4650
Wire Wire Line
	3100 4650 5450 4650
$EndSCHEMATC

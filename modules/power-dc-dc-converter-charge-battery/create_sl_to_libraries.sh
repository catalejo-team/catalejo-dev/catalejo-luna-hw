mkdir -p libraries
PATH_KICAD_LIB=../../kicad-libraries
MY_KICAD_LIB=$PATH_KICAD_LIB/my-components
SNAPEDA_KICAD_LIB=$PATH_KICAD_LIB/snapeda
SMISIOTO_KICAD_LIB=$PATH_KICAD_LIB/smisioto
COMPONENTSEARCHENGINE_KICAD_LIB=$PATH_KICAD_LIB/componentsearchengine

# ln -sr $PATH_KICAD_LIB/kicad-ESP8266 ./libraries/kicad-ESP8266
# ln -sr $MY_KICAD_LIB/drv8830 ./libraries
ln -sr $MY_KICAD_LIB/fdt434p ./libraries
ln -sr $MY_KICAD_LIB/charger_tp4056 ./libraries
ln -sr $MY_KICAD_LIB/mcp1826 ./libraries
ln -sr $SNAPEDA_KICAD_LIB/mt3608 ./libraries
ln -sr $SNAPEDA_KICAD_LIB/ss34 ./libraries
ln -sr $SNAPEDA_KICAD_LIB/dw01a ./libraries
ln -sr $SMISIOTO_KICAD_LIB/walter/smd_inductors ./libraries
ln -sr $COMPONENTSEARCHENGINE_KICAD_LIB/FS8205A ./libraries
# ln -sr $MY_KICAD_LIB/motor_driver ./libraries

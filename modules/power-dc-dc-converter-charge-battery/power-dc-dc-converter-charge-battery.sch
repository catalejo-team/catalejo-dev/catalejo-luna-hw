EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:USB_B_Micro J?
U 1 1 5EACBC62
P 1150 2100
F 0 "J?" H 1207 2567 50  0000 C CNN
F 1 "USB_B_Micro" H 1207 2476 50  0000 C CNN
F 2 "" H 1300 2050 50  0001 C CNN
F 3 "~" H 1300 2050 50  0001 C CNN
	1    1150 2100
	1    0    0    -1  
$EndComp
Text HLabel 1600 2100 2    50   Output ~ 0
D+
Text HLabel 1600 2200 2    50   Output ~ 0
D-
Wire Wire Line
	1450 2100 1600 2100
Wire Wire Line
	1450 2200 1600 2200
NoConn ~ 1450 2300
NoConn ~ 1050 2500
Text Notes 900  1550 0    50   ~ 0
Entrada de energización\nexterna 5V\n\nA futuro se podría considerar\nun conector tipo C si es fácil\nde conseguir en el mercado\nlocal
$Comp
L Device:C C?
U 1 1 5EAD73B0
P 2050 2350
F 0 "C?" H 2165 2396 50  0000 L CNN
F 1 "10uF" H 2165 2305 50  0000 L CNN
F 2 "" H 2088 2200 50  0001 C CNN
F 3 "~" H 2050 2350 50  0001 C CNN
	1    2050 2350
	1    0    0    -1  
$EndComp
Text Notes 1300 2750 0    50   ~ 0
Capacitor protector\ncontra sobre picos\ndebe estar junto al\nusb micro b
Text Notes 2200 3100 0    50   ~ 0
Led indicador de energización\nexterna y elemento que descarga\nel condensador de 10u
$Comp
L Device:LED D?
U 1 1 5EAE220D
P 2550 2550
F 0 "D?" V 2589 2432 50  0000 R CNN
F 1 "RED" V 2498 2432 50  0000 R CNN
F 2 "" H 2550 2550 50  0001 C CNN
F 3 "~" H 2550 2550 50  0001 C CNN
	1    2550 2550
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5EAE1D12
P 2550 2150
F 0 "R?" H 2620 2196 50  0000 L CNN
F 1 "1k" H 2620 2105 50  0000 L CNN
F 2 "" V 2480 2150 50  0001 C CNN
F 3 "~" H 2550 2150 50  0001 C CNN
	1    2550 2150
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 5EAF8EEB
P 4600 3000
F 0 "R?" V 4393 3000 50  0000 C CNN
F 1 "100k" V 4484 3000 50  0000 C CNN
F 2 "" V 4530 3000 50  0001 C CNN
F 3 "~" H 4600 3000 50  0001 C CNN
	1    4600 3000
	-1   0    0    1   
$EndComp
$Comp
L fdt434p:FDT434P Q?
U 1 1 5EAFB19D
P 5100 2350
F 0 "Q?" H 5208 2396 50  0000 L CNN
F 1 "FDT434P" H 5208 2305 50  0000 L CNN
F 2 "SOT230P700X180-4N" H 5100 2350 50  0001 L BNN
F 3 "Fairchild Semiconductor" H 5100 2350 50  0001 L BNN
	1    5100 2350
	1    0    0    -1  
$EndComp
$Comp
L Diode:1N5819 D?
U 1 1 5EAFE3DF
P 4850 2750
F 0 "D?" H 4850 2534 50  0000 C CNN
F 1 "1N5819" H 4850 2625 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 4850 2575 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88525/1n5817.pdf" H 4850 2750 50  0001 C CNN
	1    4850 2750
	-1   0    0    1   
$EndComp
Wire Wire Line
	4700 2750 4600 2750
Wire Wire Line
	4600 2750 4600 2250
Wire Wire Line
	4800 2250 4600 2250
Connection ~ 4600 2250
Wire Wire Line
	5000 2750 5100 2750
Wire Wire Line
	5100 2750 5100 2650
Wire Wire Line
	5100 1950 5100 2050
Text Notes 4250 1550 0    50   ~ 0
Circuito que sirve para seleccionar la fuente de energización\n(externa o batería).\nFuncionamiento: Cuando vusb está desconectado el valor en\nel gate tiende a 0 V por la resistencia de 100k, ésto hace\nque el canal del pmos cierre el circuito entre el vbatt y\nv_sw_power. Cuando el voltaje vbus es 5v el canal del pmos\nabre el circuito entre vbatt y v_sw_power, sin embargo, la\ntensión percibida en el diodo cschotty es en directo,\nentrando éste en conducción y así alimentar a v_sw_power.\nEn ambos casos la tensión em v_sw_power será lo\nsuficientemente grande para que el LDO de 3.3 V entre en\nfuncionamiento.
$Comp
L mcp1826t:MCP1826T-ADJE_DC U?
U 1 1 5EB1CE1D
P 8850 1800
F 0 "U?" H 8875 2215 50  0000 C CNN
F 1 "MCP1826T-ADJE_DC" H 8875 2124 50  0000 C CNN
F 2 "SOT230P700X180-6N" H 8900 1800 50  0001 L BNN
F 3 "Microchip" H 8900 1800 50  0001 L BNN
F 4 "SOT223-5" H 8900 1800 50  0001 L BNN "Field4"
F 5 "MCP1826T-ADJE/DC" H 8900 1800 50  0001 L BNN "Field5"
F 6 "08N6496" H 8900 1800 50  0001 L BNN "Field6"
F 7 "1578432" H 8900 1800 50  0001 L BNN "Field7"
	1    8850 1800
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_DIP_x01 SW?
U 1 1 5EB22369
P 5500 2950
F 0 "SW?" H 5500 3217 50  0000 C CNN
F 1 "switch_on" H 5500 3126 50  0000 C CNN
F 2 "" H 5500 2950 50  0001 C CNN
F 3 "~" H 5500 2950 50  0001 C CNN
	1    5500 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 2950 5900 2950
Text Notes 5500 2600 0    50   ~ 0
Interruptor de encendido y \nde apagado de la placa
$Comp
L Device:C C?
U 1 1 5EB37FC1
P 7650 2200
F 0 "C?" V 7902 2200 50  0000 C CNN
F 1 "10u" V 7811 2200 50  0000 C CNN
F 2 "" H 7688 2050 50  0001 C CNN
F 3 "~" H 7650 2200 50  0001 C CNN
	1    7650 2200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5EB387CE
P 8150 1900
F 0 "R?" V 7943 1900 50  0000 C CNN
F 1 "10k" V 8034 1900 50  0000 C CNN
F 2 "" V 8080 1900 50  0001 C CNN
F 3 "~" H 8150 1900 50  0001 C CNN
	1    8150 1900
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5EB3B726
P 9450 2200
F 0 "C?" H 9565 2246 50  0000 L CNN
F 1 "1u" H 9565 2155 50  0000 L CNN
F 2 "" H 9488 2050 50  0001 C CNN
F 3 "~" H 9450 2200 50  0001 C CNN
	1    9450 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9450 2350 9450 2450
Wire Wire Line
	9350 1950 9450 1950
Connection ~ 9450 1950
Wire Wire Line
	9450 1950 9450 2050
Wire Wire Line
	8400 1800 8300 1800
Wire Wire Line
	8400 2200 8300 2200
Wire Wire Line
	8300 2200 8300 2100
Wire Wire Line
	8300 2100 8400 2100
Text Notes 8400 2700 0    50   ~ 0
TODO: completar los datos del LDO\nLDO 3V3 a 1 amp
Text Notes 9550 2500 0    50   ~ 0
Reviar el valor\ndel condensador\n!!!
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 5EB5A2BE
P 6750 5350
F 0 "J?" H 6830 5342 50  0000 L CNN
F 1 "Battery" H 6830 5251 50  0000 L CNN
F 2 "" H 6750 5350 50  0001 C CNN
F 3 "~" H 6750 5350 50  0001 C CNN
	1    6750 5350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5EAFBD2D
P 6150 5400
F 0 "C?" H 6265 5446 50  0000 L CNN
F 1 "10u" H 6265 5355 50  0000 L CNN
F 2 "" H 6188 5250 50  0001 C CNN
F 3 "~" H 6150 5400 50  0001 C CNN
	1    6150 5400
	1    0    0    -1  
$EndComp
Text Notes 10150 1050 0    50   ~ 0
Definir esquema\ncorrecto para\ninterruptor !!!
Text Notes 4800 3400 0    50   ~ 0
Definir el esquema\ncorrecto para el\nswitch de encendido\nde placa !!!
Text HLabel 10300 4200 2    50   Output ~ 0
+5V
Text Notes 10150 1550 0    50   ~ 0
éste selector\npermite cambiar \nel nivel de tensión\npara los gpio\nrequeridos
Text Label 2050 2900 3    50   ~ 0
GND
Text Label 2050 1800 1    50   ~ 0
VUSB
Text Label 4600 1950 1    50   ~ 0
VUSB
Text Label 4600 3250 3    50   ~ 0
GND
Text Label 5100 1950 1    50   ~ 0
+VBATT
Wire Wire Line
	5100 2750 5100 2950
Wire Wire Line
	5100 2950 5200 2950
Connection ~ 5100 2750
Wire Wire Line
	2550 2000 2550 1900
Wire Wire Line
	2550 2700 2550 2800
Wire Wire Line
	2550 2800 2050 2800
Wire Wire Line
	2050 2800 2050 2500
Wire Wire Line
	2050 2200 2050 1900
Connection ~ 2050 1900
Wire Wire Line
	2050 1900 2550 1900
Wire Wire Line
	2550 2400 2550 2300
Wire Wire Line
	1150 2500 1150 2800
Wire Wire Line
	1150 2800 2050 2800
Connection ~ 2050 2800
Wire Wire Line
	2050 2900 2050 2800
Wire Wire Line
	1450 1900 2050 1900
Wire Wire Line
	2050 1800 2050 1900
Text Label 5900 2850 1    50   ~ 0
VIN
Wire Wire Line
	5900 2850 5900 2950
Connection ~ 8300 2200
Text Label 7650 1100 1    50   ~ 0
VIN
Text Label 9450 1100 1    50   ~ 0
+3.3V
Wire Wire Line
	8300 1600 8400 1600
Wire Wire Line
	8300 1800 8300 1600
Text Label 8300 2550 3    50   ~ 0
GND
Wire Wire Line
	7650 2350 7650 2450
Wire Wire Line
	7650 2450 8300 2450
Wire Wire Line
	8300 2200 8300 2450
Connection ~ 8300 2450
Wire Wire Line
	8300 2550 8300 2450
Wire Wire Line
	8300 2450 9450 2450
$Comp
L tp4056:TP4056 U?
U 1 1 5ECD10D3
P 2550 6550
F 0 "U?" H 2525 7065 50  0000 C CNN
F 1 "TP4056" H 2525 6974 50  0000 C CNN
F 2 "SOP-8" H 2550 6550 50  0001 L BNN
F 3 "" H 2550 6550 50  0001 C CNN
	1    2550 6550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5ECD1123
P 1550 6900
F 0 "R?" H 1620 6946 50  0000 L CNN
F 1 "1k" H 1620 6855 50  0000 L CNN
F 2 "" V 1480 6900 50  0001 C CNN
F 3 "~" H 1550 6900 50  0001 C CNN
	1    1550 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 6350 1950 6350
Wire Wire Line
	1950 6350 1950 6250
Wire Wire Line
	1950 6250 2050 6250
Connection ~ 1950 6250
$Comp
L Device:R R?
U 1 1 5ECD1147
P 3100 6900
F 0 "R?" H 3170 6946 50  0000 L CNN
F 1 "1.2k" H 3170 6855 50  0000 L CNN
F 2 "" V 3030 6900 50  0001 C CNN
F 3 "~" H 3100 6900 50  0001 C CNN
	1    3100 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 6750 3100 6650
Wire Wire Line
	3100 6650 3000 6650
Text Notes 1100 5800 0    50   ~ 0
Estado de cargador \nCHRG -> RED LED \nSTDBY -> GREEN LED\n\nEstado          | R | G\nCargando       | 1 | 0\nChrg terminada | 0 | 1\nVin = 0V       | 0 | 0\nNo battery      | 1 | blink
$Comp
L Device:R R?
U 1 1 5ECF4DF2
P 1100 6900
F 0 "R?" H 1170 6946 50  0000 L CNN
F 1 "1k" H 1170 6855 50  0000 L CNN
F 2 "" V 1030 6900 50  0001 C CNN
F 3 "~" H 1100 6900 50  0001 C CNN
	1    1100 6900
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 5ECD112D
P 1550 6500
F 0 "D?" V 1589 6382 50  0000 R CNN
F 1 "RED" V 1498 6382 50  0000 R CNN
F 2 "" H 1550 6500 50  0001 C CNN
F 3 "~" H 1550 6500 50  0001 C CNN
	1    1550 6500
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D?
U 1 1 5ECD1137
P 1100 6500
F 0 "D?" V 1139 6382 50  0000 R CNN
F 1 "GREEN" V 1048 6382 50  0000 R CNN
F 2 "" H 1100 6500 50  0001 C CNN
F 3 "~" H 1100 6500 50  0001 C CNN
	1    1100 6500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1550 6250 1550 6350
Connection ~ 1550 6250
Wire Wire Line
	1550 6250 1950 6250
Wire Wire Line
	1550 6650 1550 6750
Wire Wire Line
	2050 6550 1850 6550
Wire Wire Line
	1850 6550 1850 7150
Wire Wire Line
	1850 7150 1550 7150
Wire Wire Line
	1550 7150 1550 7050
Wire Wire Line
	1100 6650 1100 6750
Wire Wire Line
	1100 6350 1100 6250
Wire Wire Line
	1100 6250 1550 6250
Text Label 750  6150 1    50   ~ 0
VUSB
Connection ~ 1100 6250
Wire Wire Line
	1100 7250 1950 7250
Wire Wire Line
	1950 7250 1950 6650
Wire Wire Line
	1950 6650 2050 6650
Wire Wire Line
	1100 7050 1100 7250
Text Label 750  7450 3    50   ~ 0
GND
Text Label 6450 4250 1    50   ~ 0
+VBATT
Wire Wire Line
	3000 6550 3400 6550
Wire Wire Line
	4600 3150 4600 3250
Wire Wire Line
	4600 2850 4600 2750
Connection ~ 4600 2750
Text HLabel 10150 2150 2    50   Output ~ 0
GND
Text HLabel 10150 1950 2    50   Output ~ 0
+3.3V
Wire Wire Line
	9450 1300 7900 1300
Wire Wire Line
	9450 1300 9450 1950
Wire Wire Line
	8400 1900 8300 1900
Wire Wire Line
	8000 1900 7900 1900
Wire Wire Line
	7900 1900 7900 1300
Wire Wire Line
	9450 1100 9450 1300
Connection ~ 9450 1300
Wire Wire Line
	7650 1100 7650 1200
Connection ~ 7650 1200
Wire Wire Line
	10150 2150 10050 2150
Wire Wire Line
	10050 2150 10050 2450
Wire Wire Line
	10050 2450 9450 2450
Connection ~ 9450 2450
Wire Wire Line
	7650 1200 7650 2050
Wire Wire Line
	8300 1600 8300 1200
Connection ~ 8300 1600
Wire Wire Line
	8300 1200 7650 1200
Wire Wire Line
	4600 1950 4600 2250
$Comp
L mt3608:MT3608 U?
U 1 1 5EAED95E
P 8550 4950
F 0 "U?" H 8550 5415 50  0000 C CNN
F 1 "MT3608" H 8550 5324 50  0000 C CNN
F 2 "SOT95P280X145-6N" H 8550 4950 50  0001 L BNN
F 3 "1.45mm" H 8550 4950 50  0001 L BNN
F 4 "1.0" H 8550 4950 50  0001 L BNN "Field4"
F 5 "Aero Semi" H 8550 4950 50  0001 L BNN "Field5"
F 6 "IPC-7351B" H 8550 4950 50  0001 L BNN "Field6"
	1    8550 4950
	1    0    0    -1  
$EndComp
$Comp
L Device:Ferrite_Bead FB?
U 1 1 5EAF0860
P 8550 4200
F 0 "FB?" V 8276 4200 50  0000 C CNN
F 1 "22uH" V 8367 4200 50  0000 C CNN
F 2 "" V 8480 4200 50  0001 C CNN
F 3 "~" H 8550 4200 50  0001 C CNN
	1    8550 4200
	0    1    1    0   
$EndComp
$Comp
L ss34:SS34 D?
U 1 1 5EAF6531
P 9550 4200
F 0 "D?" H 9550 4417 50  0000 C CNN
F 1 "SS34" H 9550 4326 50  0000 C CNN
F 2 "DIOM7959X265N" H 9550 4200 50  0001 L BNN
F 3 "31 Aug 2016" H 9550 4200 50  0001 L BNN
F 4 "36301" H 9550 4200 50  0001 L BNN "Field4"
F 5 "On Semiconductor" H 9550 4200 50  0001 L BNN "Field5"
F 6 "2.65mm" H 9550 4200 50  0001 L BNN "Field6"
F 7 "IPC-7351B" H 9550 4200 50  0001 L BNN "Field7"
	1    9550 4200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5EAF8651
P 7850 5200
F 0 "C?" H 7965 5246 50  0000 L CNN
F 1 "22u" H 7965 5155 50  0000 L CNN
F 2 "" H 7888 5050 50  0001 C CNN
F 3 "~" H 7850 5200 50  0001 C CNN
	1    7850 5200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5EAF91A9
P 10200 4850
F 0 "C?" H 10315 4896 50  0000 L CNN
F 1 "22u" H 10315 4805 50  0000 L CNN
F 2 "" H 10238 4700 50  0001 C CNN
F 3 "~" H 10200 4850 50  0001 C CNN
	1    10200 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5EAF97CA
P 9850 4600
F 0 "R?" H 9920 4646 50  0000 L CNN
F 1 "7.2k" H 9920 4555 50  0000 L CNN
F 2 "" V 9780 4600 50  0001 C CNN
F 3 "~" H 9850 4600 50  0001 C CNN
	1    9850 4600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5EAF9AFC
P 9850 5100
F 0 "R?" H 9920 5146 50  0000 L CNN
F 1 "1k" H 9920 5055 50  0000 L CNN
F 2 "" V 9780 5100 50  0001 C CNN
F 3 "~" H 9850 5100 50  0001 C CNN
	1    9850 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 4750 9250 4750
Wire Wire Line
	9250 4750 9250 4200
Wire Wire Line
	9350 4200 9250 4200
Wire Wire Line
	9850 4750 9850 4850
Wire Wire Line
	9850 4850 9150 4850
Wire Wire Line
	9850 4950 9850 4850
Connection ~ 9850 4850
Wire Wire Line
	9850 4450 9850 4200
Wire Wire Line
	9850 4200 9750 4200
Wire Wire Line
	8700 4200 9250 4200
Connection ~ 9250 4200
Wire Wire Line
	10200 4700 10200 4200
Wire Wire Line
	10200 4200 9850 4200
Connection ~ 9850 4200
Wire Wire Line
	7850 5050 7850 4950
Wire Wire Line
	7850 4950 7950 4950
Wire Wire Line
	7950 4750 7850 4750
Wire Wire Line
	7850 4750 7850 4950
Connection ~ 7850 4950
Wire Wire Line
	7850 5350 7850 5450
Wire Wire Line
	7850 5450 9250 5450
Wire Wire Line
	10200 5450 10200 5000
Wire Wire Line
	9850 5250 9850 5450
Connection ~ 9850 5450
Wire Wire Line
	9850 5450 10200 5450
Wire Wire Line
	9150 5150 9250 5150
Wire Wire Line
	9250 5150 9250 5450
Connection ~ 9250 5450
Wire Wire Line
	9250 5450 9850 5450
Wire Wire Line
	8400 4200 7850 4200
Wire Wire Line
	7850 4200 7850 4750
Connection ~ 7850 4750
Text Label 7750 5550 3    50   ~ 0
GND
Wire Wire Line
	7750 5550 7750 5450
Wire Wire Line
	7750 5450 7850 5450
Connection ~ 7850 5450
Text Label 7750 4100 1    50   ~ 0
VIN
Wire Wire Line
	7750 4100 7750 4200
Wire Wire Line
	7750 4200 7850 4200
Connection ~ 7850 4200
Wire Wire Line
	9450 1950 10150 1950
Wire Wire Line
	10300 4200 10200 4200
Connection ~ 10200 4200
Text Notes 9150 5900 0    50   ~ 0
Vout = 0.6*(1+R1/R2)\nR1/R2=7.33 para Vout = 5V\nR1 = 1K\nR2 = 7.2K
Text Notes 8300 3850 0    50   ~ 0
Ferrita de 2A
Text Notes 9250 3850 0    50   ~ 0
Corriente máxima 2A
$Comp
L fs8205:FS8205A Q?
U 1 1 5EB08297
P 5150 6600
F 0 "Q?" H 5750 6035 50  0000 C CNN
F 1 "FS8205A" H 5750 6126 50  0000 C CNN
F 2 "SOP65P640X120-8N" H 6200 6700 50  0001 L CNN
F 3 "http://www.ic-fortune.com/upload/Download/FS8205A-DS-12_EN.pdf" H 6200 6600 50  0001 L CNN
F 4 "Dual N-Channel Enahncement Mode Power MOSFET" H 6200 6500 50  0001 L CNN "Description"
F 5 "1.2" H 6200 6400 50  0001 L CNN "Height"
F 6 "fortune" H 6200 6300 50  0001 L CNN "Manufacturer_Name"
F 7 "FS8205A" H 6200 6200 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 6200 6100 50  0001 L CNN "Mouser Part Number"
F 9 "" H 6200 6000 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 6200 5900 50  0001 L CNN "RS Part Number"
F 11 "" H 6200 5800 50  0001 L CNN "RS Price/Stock"
	1    5150 6600
	-1   0    0    1   
$EndComp
Text Label 6450 6600 3    50   ~ 0
-VBATT
$Comp
L Device:R R?
U 1 1 5EB9161E
P 3650 6000
F 0 "R?" H 3720 6046 50  0000 L CNN
F 1 "1k" H 3720 5955 50  0000 L CNN
F 2 "" V 3580 6000 50  0001 C CNN
F 3 "~" H 3650 6000 50  0001 C CNN
	1    3650 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 6150 3650 6500
Wire Wire Line
	3950 6500 3850 6500
Wire Wire Line
	3850 6500 3850 6400
Wire Wire Line
	3850 6400 3950 6400
Wire Wire Line
	3850 6500 3650 6500
Connection ~ 3850 6500
Connection ~ 3650 6500
NoConn ~ 3950 6600
NoConn ~ 5150 6600
$Comp
L fw01a:DW01A IC?
U 1 1 5EBCEA91
P 4700 5150
F 0 "IC?" H 4700 5717 50  0000 C CNN
F 1 "DW01A" H 4700 5626 50  0000 C CNN
F 2 "SOT23-6" H 4700 5150 50  0001 L BNN
F 3 "C351410" H 4700 5150 50  0001 L BNN
F 4 "DW01A" H 4700 5150 50  0001 L BNN "Field4"
F 5 "DW01A" H 4700 5150 50  0001 L BNN "Field5"
	1    4700 5150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3650 5850 3650 4850
Wire Wire Line
	3950 6300 3850 6300
Wire Wire Line
	5150 6300 5250 6300
Wire Wire Line
	5250 6300 5250 5950
Wire Wire Line
	5150 6500 5250 6500
Wire Wire Line
	5250 6500 5250 6400
Wire Wire Line
	5250 6400 5150 6400
Connection ~ 5250 6500
Wire Wire Line
	3000 6250 3100 6250
$Comp
L Device:C C?
U 1 1 5EC204B9
P 5750 5150
F 0 "C?" H 5865 5196 50  0000 L CNN
F 1 "0.1" H 5865 5105 50  0000 L CNN
F 2 "" H 5788 5000 50  0001 C CNN
F 3 "~" H 5750 5150 50  0001 C CNN
	1    5750 5150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5EC6B789
P 5750 4600
F 0 "R?" H 5680 4554 50  0000 R CNN
F 1 "100" H 5680 4645 50  0000 R CNN
F 2 "" V 5680 4600 50  0001 C CNN
F 3 "~" H 5750 4600 50  0001 C CNN
	1    5750 4600
	-1   0    0    1   
$EndComp
Wire Wire Line
	4100 5150 3850 5150
Wire Wire Line
	3850 5150 3850 6300
Wire Wire Line
	4100 4850 3650 4850
Wire Wire Line
	4000 5950 4000 5450
Wire Wire Line
	4000 5450 4100 5450
Wire Wire Line
	4000 5950 5250 5950
NoConn ~ 5300 5150
Wire Wire Line
	5750 4850 5750 5000
Wire Wire Line
	5300 4850 5750 4850
Wire Wire Line
	5750 5450 5750 5300
Wire Wire Line
	5300 5450 5750 5450
Wire Wire Line
	5250 6500 5750 6500
Wire Wire Line
	5750 5450 5750 6500
Connection ~ 5750 5450
Wire Wire Line
	5750 4450 5750 4350
Wire Wire Line
	5750 4750 5750 4850
Connection ~ 5750 4850
Wire Wire Line
	3100 4350 3100 6250
Wire Wire Line
	3100 4350 5750 4350
Wire Wire Line
	6150 5250 6150 4350
Wire Wire Line
	6150 4350 5750 4350
Connection ~ 5750 4350
Wire Wire Line
	6550 5450 6450 5450
Wire Wire Line
	6450 5450 6450 6500
Wire Wire Line
	6450 6500 5750 6500
Connection ~ 5750 6500
Wire Wire Line
	6450 6500 6450 6600
Connection ~ 6450 6500
Wire Wire Line
	6550 5350 6450 5350
Wire Wire Line
	6450 5350 6450 4350
Wire Wire Line
	6450 4350 6150 4350
Connection ~ 6150 4350
Wire Wire Line
	6450 4250 6450 4350
Connection ~ 6450 4350
$Comp
L Device:C C?
U 1 1 5ED3B23F
P 750 6700
F 0 "C?" H 865 6746 50  0000 L CNN
F 1 "0.1u" H 865 6655 50  0000 L CNN
F 2 "" H 788 6550 50  0001 C CNN
F 3 "~" H 750 6700 50  0001 C CNN
	1    750  6700
	1    0    0    -1  
$EndComp
Wire Wire Line
	750  6550 750  6250
Wire Wire Line
	750  6250 1100 6250
Wire Wire Line
	750  6850 750  7350
Wire Wire Line
	750  7350 2550 7350
Connection ~ 3650 7350
Wire Wire Line
	3650 7350 6150 7350
Connection ~ 3400 7350
Wire Wire Line
	3400 7350 3650 7350
Connection ~ 2550 7350
Wire Wire Line
	2550 7350 3100 7350
Connection ~ 3100 7350
Wire Wire Line
	3100 7350 3400 7350
Wire Wire Line
	2550 6900 2550 7350
Wire Wire Line
	3100 7050 3100 7350
Wire Wire Line
	3400 6550 3400 7350
Wire Wire Line
	3650 6500 3650 7350
Wire Wire Line
	6150 5550 6150 7350
Wire Wire Line
	750  7450 750  7350
Connection ~ 750  7350
Wire Wire Line
	750  6150 750  6250
Connection ~ 750  6250
$EndSCHEMATC

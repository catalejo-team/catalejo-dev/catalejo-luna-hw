EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L hxj8002:hxj8002 U1
U 1 1 5EB22CCE
P 5550 3750
F 0 "U1" H 5550 4165 50  0000 C CNN
F 1 "hxj8002" H 5550 4074 50  0000 C CNN
F 2 "" H 5650 3850 50  0001 C CNN
F 3 "" H 5650 3850 50  0001 C CNN
	1    5550 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5EB23205
P 4700 4050
F 0 "C1" H 4815 4096 50  0000 L CNN
F 1 "1u" H 4815 4005 50  0000 L CNN
F 2 "" H 4738 3900 50  0001 C CNN
F 3 "~" H 4700 4050 50  0001 C CNN
	1    4700 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 3900 4700 3800
Wire Wire Line
	4700 3700 5100 3700
Wire Wire Line
	4700 3800 5100 3800
Connection ~ 4700 3800
Wire Wire Line
	4700 3800 4700 3700
$Comp
L Device:C C3
U 1 1 5EB255B3
P 4450 3600
F 0 "C3" V 4198 3600 50  0000 C CNN
F 1 "0.1u" V 4289 3600 50  0000 C CNN
F 2 "" H 4488 3450 50  0001 C CNN
F 3 "~" H 4450 3600 50  0001 C CNN
	1    4450 3600
	0    1    1    0   
$EndComp
$Comp
L Device:R R5
U 1 1 5EB2645E
P 5550 3200
F 0 "R5" V 5343 3200 50  0000 C CNN
F 1 "22k" V 5434 3200 50  0000 C CNN
F 2 "" V 5480 3200 50  0001 C CNN
F 3 "~" H 5550 3200 50  0001 C CNN
	1    5550 3200
	0    1    1    0   
$EndComp
$Comp
L Device:Speaker LS1
U 1 1 5EB27763
P 6600 3700
F 0 "LS1" H 6770 3696 50  0000 L CNN
F 1 "Speaker" H 6770 3605 50  0000 L CNN
F 2 "" H 6600 3500 50  0001 C CNN
F 3 "~" H 6590 3650 50  0001 C CNN
	1    6600 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5EB29A4E
P 7300 3600
F 0 "C2" H 7415 3646 50  0000 L CNN
F 1 "0.1u" H 7415 3555 50  0000 L CNN
F 2 "" H 7338 3450 50  0001 C CNN
F 3 "~" H 7300 3600 50  0001 C CNN
	1    7300 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 4200 4700 4300
Wire Wire Line
	5000 4300 5000 3900
Wire Wire Line
	5000 3900 5100 3900
Connection ~ 4700 4300
Wire Wire Line
	5000 4300 6100 4300
Wire Wire Line
	6100 4300 6100 3900
Wire Wire Line
	6100 3900 6000 3900
Connection ~ 5000 4300
Wire Wire Line
	6400 3700 6000 3700
Wire Wire Line
	7300 4300 6100 4300
Connection ~ 6100 4300
Wire Wire Line
	6100 3600 6000 3600
Wire Wire Line
	6000 3800 6200 3800
Connection ~ 6200 3800
Wire Wire Line
	6200 3800 6400 3800
$Comp
L Device:R R2
U 1 1 5EB31016
P 4050 3600
F 0 "R2" V 3843 3600 50  0000 C CNN
F 1 "10k" V 3934 3600 50  0000 C CNN
F 2 "" V 3980 3600 50  0001 C CNN
F 3 "~" H 4050 3600 50  0001 C CNN
	1    4050 3600
	0    1    1    0   
$EndComp
Wire Wire Line
	4200 3600 4300 3600
Text HLabel 3800 3600 0    50   Input ~ 0
IN
Text HLabel 3800 4300 0    50   Input ~ 0
GND
Text HLabel 3800 2900 0    50   Input ~ 0
VCC
Wire Wire Line
	3800 2900 6100 2900
Wire Wire Line
	6100 2900 6100 3600
Connection ~ 6100 2900
Wire Wire Line
	6100 2900 7300 2900
Wire Wire Line
	3800 4300 4700 4300
Wire Wire Line
	7300 2900 7300 3450
Wire Wire Line
	4700 4300 5000 4300
Wire Wire Line
	7300 3750 7300 4300
Wire Wire Line
	3900 3600 3800 3600
Wire Wire Line
	6200 3200 6200 3800
Wire Wire Line
	5700 3200 6200 3200
Wire Wire Line
	4600 3600 4850 3600
Wire Wire Line
	5400 3200 4850 3200
Wire Wire Line
	4850 3200 4850 3600
Connection ~ 4850 3600
Wire Wire Line
	4850 3600 5100 3600
Text Notes 7100 3400 0    50   ~ 0
Debe quedar\njunto al circuito\nintegrado
Text Notes 3950 4150 0    50   ~ 0
Debe quedar\njunto al circuito\nintegrado
$EndSCHEMATC

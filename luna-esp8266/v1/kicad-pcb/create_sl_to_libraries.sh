PATH_KICAD_LIB=../../../kicad-libraries
MY_KICAD_LIB=$PATH_KICAD_LIB/my-components

ln -sr $PATH_KICAD_LIB/kicad-ESP8266 ./libraries/kicad-ESP8266
ln -sr $MY_KICAD_LIB/drv8830 ./libraries/drv8830
ln -sr $MY_KICAD_LIB/fdt434p ./libraries/fdt434p
ln -sr $MY_KICAD_LIB/charger_tp4056 ./libraries/charger_tp4056
ln -sr $MY_KICAD_LIB/mcp1826 ./libraries/mcp1826
ln -sr $MY_KICAD_LIB/motor_driver ./libraries/motor_driver

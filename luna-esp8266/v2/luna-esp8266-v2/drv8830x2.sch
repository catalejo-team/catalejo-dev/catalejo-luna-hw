EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:L_Core_Ferrite L?
U 1 1 5EE5BB6A
P 2850 1700
F 0 "L?" V 2669 1700 50  0000 C CNN
F 1 "L_Core_Ferrite" V 2750 1800 50  0000 C CNN
F 2 "" H 2850 1700 50  0001 C CNN
F 3 "~" H 2850 1700 50  0001 C CNN
	1    2850 1700
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5EE5CBEA
P 2600 1950
F 0 "C?" H 2715 1996 50  0000 L CNN
F 1 "10u" H 2715 1905 50  0000 L CNN
F 2 "" H 2638 1800 50  0001 C CNN
F 3 "~" H 2600 1950 50  0001 C CNN
	1    2600 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5EE5CEFF
P 3100 1950
F 0 "C?" H 3215 1996 50  0000 L CNN
F 1 "10u" H 3215 1905 50  0000 L CNN
F 2 "" H 3138 1800 50  0001 C CNN
F 3 "~" H 3100 1950 50  0001 C CNN
	1    3100 1950
	1    0    0    -1  
$EndComp
$Comp
L drv8830:drv8830 #U0101
U 1 1 5EE5DFFE
P 5200 2900
F 0 "#U0101" H 5200 3581 50  0000 C CNN
F 1 "drv8830" H 5200 3490 50  0000 C CNN
F 2 "Package_SO:MSOP-10-1EP_3x3mm_P0.5mm_EP1.73x1.98mm" H 5150 3150 28  0001 C CNN
F 3 "" H 5150 3150 28  0001 C CNN
	1    5200 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5EE5F1CC
P 5800 3350
F 0 "R?" H 5870 3396 50  0000 L CNN
F 1 "R" H 5870 3305 50  0000 L CNN
F 2 "" V 5730 3350 50  0001 C CNN
F 3 "~" H 5800 3350 50  0001 C CNN
	1    5800 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5EE5F7EE
P 6100 3350
F 0 "R?" H 6170 3396 50  0000 L CNN
F 1 "R" H 6170 3305 50  0000 L CNN
F 2 "" V 6030 3350 50  0001 C CNN
F 3 "~" H 6100 3350 50  0001 C CNN
	1    6100 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 3100 5800 3100
Wire Wire Line
	6100 3100 6100 3200
Wire Wire Line
	5800 3200 5800 3100
Connection ~ 5800 3100
Wire Wire Line
	5800 3100 6100 3100
Wire Wire Line
	6100 3500 6100 3600
Wire Wire Line
	6100 3600 5800 3600
Wire Wire Line
	5200 3450 5200 3600
Wire Wire Line
	5450 3450 5450 3600
Connection ~ 5450 3600
Wire Wire Line
	5450 3600 5200 3600
Wire Wire Line
	5800 3500 5800 3600
Connection ~ 5800 3600
Wire Wire Line
	5800 3600 5450 3600
Text HLabel 2150 2300 1    50   Input ~ 0
VIN
Wire Wire Line
	2600 1700 2700 1700
Wire Wire Line
	2600 1700 2600 1800
$Comp
L Device:R R?
U 1 1 5EE70C66
P 4100 2400
F 0 "R?" H 4170 2446 50  0000 L CNN
F 1 "10k" H 4170 2355 50  0000 L CNN
F 2 "" V 4030 2400 50  0001 C CNN
F 3 "~" H 4100 2400 50  0001 C CNN
	1    4100 2400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5EE71799
P 4400 2400
F 0 "R?" H 4470 2446 50  0000 L CNN
F 1 "10k" H 4470 2355 50  0000 L CNN
F 2 "" V 4330 2400 50  0001 C CNN
F 3 "~" H 4400 2400 50  0001 C CNN
	1    4400 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 2550 4400 2650
Wire Wire Line
	4400 2650 4700 2650
Wire Wire Line
	4700 2750 4100 2750
Wire Wire Line
	4100 2750 4100 2550
$Comp
L drv8830:drv8830 #U0102
U 1 1 5EE77BBA
P 5250 4750
F 0 "#U0102" H 5400 5350 50  0000 C CNN
F 1 "drv8830" H 5450 5250 50  0000 C CNN
F 2 "Package_SO:MSOP-10-1EP_3x3mm_P0.5mm_EP1.73x1.98mm" H 5200 5000 28  0001 C CNN
F 3 "" H 5200 5000 28  0001 C CNN
	1    5250 4750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5EE77BC5
P 5850 5200
F 0 "R?" H 5920 5246 50  0000 L CNN
F 1 "R" H 5920 5155 50  0000 L CNN
F 2 "" V 5780 5200 50  0001 C CNN
F 3 "~" H 5850 5200 50  0001 C CNN
	1    5850 5200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5EE77BCF
P 6150 5200
F 0 "R?" H 6220 5246 50  0000 L CNN
F 1 "R" H 6220 5155 50  0000 L CNN
F 2 "" V 6080 5200 50  0001 C CNN
F 3 "~" H 6150 5200 50  0001 C CNN
	1    6150 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 4950 5850 4950
Wire Wire Line
	6150 4950 6150 5050
Wire Wire Line
	5850 5050 5850 4950
Connection ~ 5850 4950
Wire Wire Line
	5850 4950 6150 4950
Wire Wire Line
	6150 5350 6150 5450
Wire Wire Line
	6150 5450 5850 5450
Wire Wire Line
	5250 5300 5250 5450
Wire Wire Line
	5500 5300 5500 5450
Connection ~ 5500 5450
Wire Wire Line
	5500 5450 5250 5450
Wire Wire Line
	5850 5350 5850 5450
Connection ~ 5850 5450
Wire Wire Line
	5850 5450 5500 5450
Wire Wire Line
	4400 2650 4400 4500
Wire Wire Line
	4400 4500 4750 4500
Connection ~ 4400 2650
Wire Wire Line
	4750 4600 4100 4600
Wire Wire Line
	4100 4600 4100 2750
Connection ~ 4100 2750
NoConn ~ 4700 3250
NoConn ~ 4750 5100
$Comp
L Device:R R?
U 1 1 5EE80489
P 3750 2950
F 0 "R?" V 3543 2950 50  0000 C CNN
F 1 "10K" V 3634 2950 50  0000 C CNN
F 2 "" V 3680 2950 50  0001 C CNN
F 3 "~" H 3750 2950 50  0001 C CNN
	1    3750 2950
	0    1    1    0   
$EndComp
Wire Wire Line
	3900 2950 4700 2950
Text HLabel 2050 2300 1    50   Input ~ 0
+3.3V
Text HLabel 2150 2950 3    50   Input ~ 0
GND
Wire Wire Line
	4750 4900 4650 4900
Wire Wire Line
	4650 4900 4650 4800
Wire Wire Line
	4650 4800 4750 4800
Text Label 4100 2050 1    50   ~ 0
+3.3V
Wire Wire Line
	4100 2050 4100 2150
Wire Wire Line
	4400 2250 4400 2150
Wire Wire Line
	4400 2150 4100 2150
Connection ~ 4100 2150
Wire Wire Line
	4100 2150 4100 2250
Wire Wire Line
	3000 1700 3100 1700
Wire Wire Line
	5200 1700 5200 2400
Wire Wire Line
	5250 4250 5250 4150
Wire Wire Line
	5250 4150 4000 4150
Wire Wire Line
	4000 4150 4000 1700
Connection ~ 4000 1700
Wire Wire Line
	4000 1700 5200 1700
Text HLabel 2150 2650 0    50   Input ~ 0
SDA
Text HLabel 2150 2750 0    50   Input ~ 0
SCL
Wire Wire Line
	2150 2300 2150 2400
Wire Wire Line
	2150 2400 2250 2400
Wire Wire Line
	2250 2400 2250 1700
Wire Wire Line
	2250 1700 2600 1700
Connection ~ 2600 1700
Wire Wire Line
	2050 2300 2050 2500
Wire Wire Line
	3600 2950 3500 2950
Wire Wire Line
	3500 2950 3500 2500
Wire Wire Line
	3500 2150 4100 2150
Wire Wire Line
	2050 2500 3500 2500
Connection ~ 3500 2500
Wire Wire Line
	3500 2500 3500 2150
Wire Wire Line
	3100 1800 3100 1700
Connection ~ 3100 1700
Wire Wire Line
	3100 1700 4000 1700
Wire Wire Line
	4400 2650 2150 2650
Wire Wire Line
	4100 2750 2150 2750
Wire Wire Line
	2150 2950 2150 2850
Wire Wire Line
	2150 2850 2600 2850
Wire Wire Line
	2600 2100 2600 2200
Wire Wire Line
	3100 2100 3100 2200
Wire Wire Line
	3100 2200 2600 2200
Connection ~ 2600 2200
Wire Wire Line
	2600 2200 2600 2850
Wire Wire Line
	2600 2850 2600 3050
Wire Wire Line
	2600 3600 5200 3600
Connection ~ 2600 2850
Connection ~ 5200 3600
Wire Wire Line
	5250 5450 2600 5450
Wire Wire Line
	2600 5450 2600 4900
Connection ~ 5250 5450
Connection ~ 2600 3600
Wire Wire Line
	4700 3050 2600 3050
Connection ~ 2600 3050
Wire Wire Line
	2600 3050 2600 3600
Wire Wire Line
	4650 4900 2600 4900
Connection ~ 4650 4900
Connection ~ 2600 4900
Wire Wire Line
	2600 4900 2600 3600
Text Notes 4500 2250 0    50   ~ 0
Pullup I2C
Text Label 2600 5550 3    50   ~ 0
GND
Wire Wire Line
	2600 5550 2600 5450
Connection ~ 2600 5450
Text Label 4000 1600 1    50   ~ 0
+VM
Wire Wire Line
	4000 1600 4000 1700
Text Label 2600 1600 1    50   ~ 0
VIN
Wire Wire Line
	2600 1600 2600 1700
Text Notes 4500 1450 0    50   ~ 0
Debe revisar las especificaciones\nde funcionamiento y pornerlas\nen éste esquemático !!!
$Comp
L Connector_Generic:Conn_01x04 J?
U 1 1 5EECE37B
P 7350 3850
F 0 "J?" H 7430 3842 50  0000 L CNN
F 1 "Conn_01x04" H 7430 3751 50  0000 L CNN
F 2 "" H 7350 3850 50  0001 C CNN
F 3 "~" H 7350 3850 50  0001 C CNN
	1    7350 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 4650 7050 4050
Wire Wire Line
	7050 4050 7150 4050
Wire Wire Line
	7150 3950 6900 3950
Wire Wire Line
	6900 3950 6900 4500
Wire Wire Line
	7150 3850 6900 3850
Wire Wire Line
	6900 3850 6900 2800
Wire Wire Line
	7050 2650 7050 3750
Wire Wire Line
	7050 3750 7150 3750
Wire Wire Line
	5700 2650 7050 2650
Wire Wire Line
	5700 2800 6900 2800
Wire Wire Line
	5750 4500 6900 4500
Wire Wire Line
	5750 4650 7050 4650
Text Notes 5850 5850 0    50   ~ 0
Por ahora usaré\nla siguiente ferrita\nFERR0805 600\n!!!
Text Notes 5800 4000 0    50   ~ 0
Por ahora usaré\nla siguiente ferrita\nFERR0805 600\n!!!
Text Notes 7350 3600 0    50   ~ 0
Salida para 2\nmotores
$EndSCHEMATC

EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 8900 3450 2    50   ~ 0
0
Text Label 8900 3600 2    50   ~ 0
1
Text Label 8900 3750 2    50   ~ 0
2
Text Label 8900 3900 2    50   ~ 0
3
Text Label 8900 4050 2    50   ~ 0
4
Text Label 8900 4200 2    50   ~ 0
5
Text Label 8900 4350 2    50   ~ 0
6
Text Label 8900 4500 2    50   ~ 0
7
Text Label 8900 4650 2    50   ~ 0
8
Text Label 8900 4800 2    50   ~ 0
9
Text Label 8900 4950 2    50   ~ 0
10
Text Notes 8600 3100 0    50   ~ 0
Pin -> Observación\n0 [16] -> compartido, wake up, no pwm, gpio\n1 [14] -> libre, pwm, gpio\n2 [12] -> libre, pwm, gpio\n3 [RX] -> compartido, RX, pwm, gpio\n4 [TX] -> compartido, TX, pwm, gpio\n5 [2] -> compartido: WS2812B, pull up, pwm\n6 [13] -> libre, pwm, gpio\n7 [15] -> exclusivo: PARLANTE, pull down para iniciar\n8 [0] -> exclusivo: BTN USER, pull up para iniciar\n9 [5] -> exclusivo: SDA por SW\n10[4]-> exclusivo: SCL por SW, sirve para pwm, gpio
Wire Wire Line
	3050 2650 3050 2400
Text Label 5300 3650 0    50   ~ 0
7
Text Label 5300 3550 0    50   ~ 0
5
Text Label 5300 3450 0    50   ~ 0
8
Wire Wire Line
	5300 3350 5200 3350
Text Label 5300 3350 0    50   ~ 0
10
Wire Wire Line
	5300 3250 5200 3250
Text Label 5300 3250 0    50   ~ 0
9
Wire Wire Line
	5300 3150 5200 3150
Text Label 5300 3150 0    50   ~ 0
3
Wire Wire Line
	5300 3050 5200 3050
Text Label 5300 3050 0    50   ~ 0
4
Wire Wire Line
	3400 3650 3300 3650
Text Label 3300 3650 2    50   ~ 0
6
Wire Wire Line
	3400 3550 3300 3550
Text Label 3300 3550 2    50   ~ 0
2
Wire Wire Line
	3300 3450 3400 3450
Text Label 3300 3450 2    50   ~ 0
1
Text Label 3300 3350 2    50   ~ 0
0
Connection ~ 1500 4700
Connection ~ 1500 3650
Wire Wire Line
	1500 3650 1500 4700
Connection ~ 5200 4150
Wire Wire Line
	5200 4150 5200 4700
Wire Wire Line
	750  2300 750  2400
Wire Wire Line
	750  4800 750  4700
Wire Wire Line
	750  4700 1500 4700
Text HLabel 750  4800 3    50   Input ~ 0
GND
Connection ~ 1500 2400
Wire Wire Line
	750  2400 1500 2400
Wire Wire Line
	3400 3150 3300 3150
Text Label 3300 3150 2    50   ~ 0
ADC
Text Notes 3450 4350 0    50   ~ 0
Bypass
Wire Wire Line
	3300 3750 2850 3750
Connection ~ 3300 3750
$Comp
L Device:C C?
U 1 1 5EF30BE1
P 3300 4200
F 0 "C?" H 3415 4246 50  0000 L CNN
F 1 "100p" H 3415 4155 50  0000 L CNN
F 2 "" H 3338 4050 50  0001 C CNN
F 3 "~" H 3300 4200 50  0001 C CNN
	1    3300 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 2400 3050 2400
Connection ~ 2850 2400
Wire Wire Line
	2850 3750 2850 2400
Wire Wire Line
	3400 3750 3300 3750
Wire Wire Line
	1500 2400 2850 2400
Wire Wire Line
	1500 2650 1500 2400
Wire Wire Line
	3050 3250 3050 2950
Wire Wire Line
	5200 4150 5200 3750
Wire Wire Line
	5550 4150 5200 4150
Wire Wire Line
	5550 4050 5550 4150
Wire Wire Line
	5550 3650 5550 3750
Wire Wire Line
	5200 3650 5550 3650
$Comp
L Device:R R?
U 1 1 5EF26234
P 5550 3900
F 0 "R?" H 5620 3946 50  0000 L CNN
F 1 "12k" H 5620 3855 50  0000 L CNN
F 2 "" V 5480 3900 50  0001 C CNN
F 3 "~" H 5550 3900 50  0001 C CNN
	1    5550 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 3550 5550 2900
Wire Wire Line
	5200 3550 5550 3550
$Comp
L Device:R R?
U 1 1 5EF25682
P 5550 2750
F 0 "R?" H 5620 2796 50  0000 L CNN
F 1 "12k" H 5620 2705 50  0000 L CNN
F 2 "" V 5480 2750 50  0001 C CNN
F 3 "~" H 5550 2750 50  0001 C CNN
	1    5550 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 3250 3400 3250
$Comp
L Device:R R?
U 1 1 5EF23C1C
P 3050 2800
F 0 "R?" H 3120 2846 50  0000 L CNN
F 1 "12k" H 3120 2755 50  0000 L CNN
F 2 "" V 2980 2800 50  0001 C CNN
F 3 "~" H 3050 2800 50  0001 C CNN
	1    3050 2800
	1    0    0    -1  
$EndComp
Connection ~ 1900 3050
Wire Wire Line
	1900 3050 1500 3050
Wire Wire Line
	1500 3650 1500 3450
Wire Wire Line
	1100 3650 1500 3650
Wire Wire Line
	1100 3550 1100 3650
Wire Wire Line
	1500 3050 1500 3150
Connection ~ 1500 3050
Wire Wire Line
	1100 3050 1500 3050
Wire Wire Line
	1100 3150 1100 3050
Wire Wire Line
	1500 2950 1500 3050
Text Notes 1950 3800 0    50   ~ 0
Habilitador del\nmodo de dormir\npor software
$Comp
L Switch:SW_Push SW?
U 1 1 5EF0BF6C
P 1100 3350
F 0 "SW?" V 1054 3302 50  0000 R CNN
F 1 "reset" V 1145 3302 50  0000 R CNN
F 2 "" H 1100 3550 50  0001 C CNN
F 3 "~" H 1100 3550 50  0001 C CNN
	1    1100 3350
	0    -1   1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5EF0B7F5
P 1500 3300
F 0 "C?" H 1615 3346 50  0000 L CNN
F 1 "100n" H 1615 3255 50  0000 L CNN
F 2 "" H 1538 3150 50  0001 C CNN
F 3 "~" H 1500 3300 50  0001 C CNN
	1    1500 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5EF0B4E4
P 1500 2800
F 0 "R?" H 1570 2846 50  0000 L CNN
F 1 "12k" H 1570 2755 50  0000 L CNN
F 2 "" V 1430 2800 50  0001 C CNN
F 3 "~" H 1500 2800 50  0001 C CNN
	1    1500 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 3050 3400 3050
Wire Wire Line
	1900 3350 1900 3050
Wire Wire Line
	2000 3350 1900 3350
Wire Wire Line
	3400 3350 2600 3350
$Comp
L Switch:SW_DIP_x01 SW?
U 1 1 5EF071F6
P 2300 3350
F 0 "SW?" H 2300 3617 50  0000 C CNN
F 1 "SW_DIP_x01" H 2300 3526 50  0000 C CNN
F 2 "" H 2300 3350 50  0001 C CNN
F 3 "~" H 2300 3350 50  0001 C CNN
	1    2300 3350
	1    0    0    -1  
$EndComp
NoConn ~ 4550 4250
NoConn ~ 4450 4250
NoConn ~ 4350 4250
NoConn ~ 4250 4250
NoConn ~ 4150 4250
NoConn ~ 4050 4250
Text HLabel 750  2300 1    50   Input ~ 0
+3.3V
$Comp
L esp32-8266:ESP-12E U?
U 1 1 5EF05391
P 4300 3350
F 0 "U?" H 4300 4115 50  0000 C CNN
F 1 "ESP-12E" H 4300 4024 50  0000 C CNN
F 2 "" H 4300 3350 50  0001 C CNN
F 3 "http://l0l.org.uk/2014/12/esp8266-modules-hardware-guide-gotta-catch-em-all/" H 4300 3350 50  0001 C CNN
	1    4300 3350
	1    0    0    -1  
$EndComp
Connection ~ 3050 2400
Wire Wire Line
	3050 2400 5550 2400
Connection ~ 6100 4050
Wire Wire Line
	6100 4700 6100 4050
Wire Wire Line
	6100 3050 6100 2400
Wire Wire Line
	6100 4050 6100 3850
Wire Wire Line
	6700 4050 6100 4050
Wire Wire Line
	6700 3950 6700 4050
Connection ~ 6100 3450
Wire Wire Line
	6100 3450 6100 3550
Wire Wire Line
	6100 3350 6100 3450
Wire Wire Line
	6700 3450 6100 3450
Wire Wire Line
	6700 3550 6700 3450
Text Notes 6550 3400 0    50   ~ 0
botón de\nusuario
$Comp
L Switch:SW_Push SW?
U 1 1 5EF19695
P 6700 3750
F 0 "SW?" V 6654 3702 50  0000 R CNN
F 1 "flash" V 6745 3702 50  0000 R CNN
F 2 "" H 6700 3950 50  0001 C CNN
F 3 "~" H 6700 3950 50  0001 C CNN
	1    6700 3750
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5EF1968B
P 6100 3700
F 0 "C?" H 6215 3746 50  0000 L CNN
F 1 "100n" H 6215 3655 50  0000 L CNN
F 2 "" H 6138 3550 50  0001 C CNN
F 3 "~" H 6100 3700 50  0001 C CNN
	1    6100 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5EF19681
P 6100 3200
F 0 "R?" H 6170 3246 50  0000 L CNN
F 1 "12k" H 6170 3155 50  0000 L CNN
F 2 "" V 6030 3200 50  0001 C CNN
F 3 "~" H 6100 3200 50  0001 C CNN
	1    6100 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3450 6100 3450
Wire Wire Line
	5550 2400 5550 2600
Wire Wire Line
	5550 2400 6100 2400
Connection ~ 5550 2400
Wire Wire Line
	5200 4700 6100 4700
Connection ~ 5200 4700
Text HLabel 9000 3300 2    50   Input ~ 0
A0
Text Label 8900 3300 2    50   ~ 0
ADC_EXT
Wire Wire Line
	9000 3300 8900 3300
Text HLabel 9000 3450 2    50   BiDi ~ 0
16
Text HLabel 9000 3600 2    50   BiDi ~ 0
14
Text HLabel 9000 3750 2    50   BiDi ~ 0
12
Text HLabel 9000 3900 2    50   BiDi ~ 0
RX
Text HLabel 9000 4050 2    50   BiDi ~ 0
TX
Text HLabel 9000 4200 2    50   BiDi ~ 0
2
Text HLabel 9000 4350 2    50   BiDi ~ 0
13
Text HLabel 9000 4500 2    50   BiDi ~ 0
15
Text HLabel 9000 4650 2    50   BiDi ~ 0
0
Text HLabel 9000 4800 2    50   BiDi ~ 0
5
Text HLabel 9000 4950 2    50   BiDi ~ 0
4
Wire Wire Line
	9000 3450 8900 3450
Wire Wire Line
	9000 3600 8900 3600
Wire Wire Line
	9000 3750 8900 3750
Wire Wire Line
	9000 3900 8900 3900
Wire Wire Line
	9000 4050 8900 4050
Wire Wire Line
	9000 4200 8900 4200
Wire Wire Line
	9000 4350 8900 4350
Wire Wire Line
	9000 4500 8900 4500
Wire Wire Line
	9000 4650 8900 4650
Wire Wire Line
	9000 4800 8900 4800
Wire Wire Line
	9000 4950 8900 4950
Wire Wire Line
	1500 4700 3300 4700
Wire Wire Line
	3300 4350 3300 4700
Connection ~ 3300 4700
Wire Wire Line
	3300 4700 5200 4700
Wire Wire Line
	3300 3750 3300 4050
$Comp
L Device:R R?
U 1 1 5EB45036
P 7300 3550
F 0 "R?" H 7370 3596 50  0000 L CNN
F 1 "220K" H 7370 3505 50  0000 L CNN
F 2 "" V 7230 3550 50  0001 C CNN
F 3 "~" H 7300 3550 50  0001 C CNN
	1    7300 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5EB453ED
P 7300 4050
F 0 "R?" H 7370 4096 50  0000 L CNN
F 1 "100K" H 7370 4005 50  0000 L CNN
F 2 "" V 7230 4050 50  0001 C CNN
F 3 "~" H 7300 4050 50  0001 C CNN
	1    7300 4050
	1    0    0    -1  
$EndComp
Text Label 7200 3800 2    50   ~ 0
ADC
Wire Wire Line
	7300 3700 7300 3800
Wire Wire Line
	7300 3800 7200 3800
Wire Wire Line
	7300 3900 7300 3800
Connection ~ 7300 3800
Text Label 3300 4800 3    50   ~ 0
GND
Wire Wire Line
	3300 4800 3300 4700
Text Notes 7600 3850 0    50   ~ 0
1% de\ntolerancia
Text Label 7300 4300 3    50   ~ 0
GND
Wire Wire Line
	7300 4200 7300 4300
Text Label 7300 3300 1    50   ~ 0
ADC_EXT
Wire Wire Line
	7300 3300 7300 3400
$EndSCHEMATC

EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 3550 3550 800  650 
U 5EACB808
F0 "power" 50
F1 "power.sch" 50
F2 "D+" O R 4350 3650 50 
F3 "D-" O R 4350 3750 50 
F4 "VCC" O R 4350 3900 50 
F5 "GND" O R 4350 4100 50 
F6 "+3.3V" O R 4350 4000 50 
$EndSheet
$Sheet
S 6150 2700 800  650 
U 5EB97326
F0 "drv8830x2motores" 50
F1 "drv8830x2.sch" 50
F2 "VIN" I L 6150 3050 50 
F3 "+3.3V" I L 6150 3150 50 
F4 "GND" I L 6150 3250 50 
F5 "SDA" I L 6150 2900 50 
F6 "SCL" I L 6150 2800 50 
$EndSheet
$Sheet
S 6150 3600 800  400 
U 5EEF08E0
F0 "led_rgb_ws2812b" 50
F1 "rgb_ws2812b.sch" 50
F2 "+3.3V" I L 6150 3800 50 
F3 "GND" I L 6150 3900 50 
F4 "DIN_SERIAL" I L 6150 3700 50 
$EndSheet
$Sheet
S 6150 4300 800  400 
U 5EEF47EA
F0 "parlante" 50
F1 "speaker.sch" 50
F2 "INPUT_SOUND" I L 6150 4400 50 
F3 "VIN" I L 6150 4500 50 
F4 "GND" I L 6150 4600 50 
$EndSheet
$Sheet
S 4850 3550 650  550 
U 5EF04C93
F0 "esp8266" 50
F1 "esp8266.sch" 50
$EndSheet
$EndSCHEMATC

EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Transistor_FET:BSS138 Q?
U 1 1 5EEF49AD
P 6250 3300
F 0 "Q?" H 6454 3346 50  0000 L CNN
F 1 "BSS138" H 6454 3255 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6450 3225 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/BS/BSS138.pdf" H 6250 3300 50  0001 L CNN
	1    6250 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 3000 6350 3000
Wire Wire Line
	6350 3000 6350 3100
$Comp
L Device:R R?
U 1 1 5EEF6A66
P 5950 3550
F 0 "R?" H 6020 3596 50  0000 L CNN
F 1 "200k" H 6020 3505 50  0000 L CNN
F 2 "" V 5880 3550 50  0001 C CNN
F 3 "~" H 5950 3550 50  0001 C CNN
	1    5950 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 3700 5950 3800
Wire Wire Line
	5950 3800 6350 3800
Wire Wire Line
	6350 3800 6350 3500
Wire Wire Line
	6050 3300 5950 3300
Wire Wire Line
	5950 3300 5950 3400
$Comp
L Device:R R?
U 1 1 5EEF7B44
P 5700 3300
F 0 "R?" V 5493 3300 50  0000 C CNN
F 1 "100k" V 5584 3300 50  0000 C CNN
F 2 "" V 5630 3300 50  0001 C CNN
F 3 "~" H 5700 3300 50  0001 C CNN
	1    5700 3300
	0    1    1    0   
$EndComp
Wire Wire Line
	5850 3300 5950 3300
Connection ~ 5950 3300
Text HLabel 5450 3300 0    50   Input ~ 0
INPUT_SOUND
Text HLabel 5450 2800 1    50   Input ~ 0
VIN
Text HLabel 5450 3900 3    50   Input ~ 0
GND
Connection ~ 5950 3800
Wire Wire Line
	5450 3300 5550 3300
Wire Wire Line
	5450 3900 5450 3800
Wire Wire Line
	5450 3800 5950 3800
Wire Wire Line
	5450 2800 5450 2900
Wire Wire Line
	5450 2900 6450 2900
$Comp
L Device:Speaker LS?
U 1 1 5EF00789
P 6650 2900
F 0 "LS?" H 6820 2896 50  0000 L CNN
F 1 "Speaker" H 6820 2805 50  0000 L CNN
F 2 "" H 6650 2700 50  0001 C CNN
F 3 "~" H 6640 2850 50  0001 C CNN
	1    6650 2900
	1    0    0    -1  
$EndComp
$EndSCHEMATC

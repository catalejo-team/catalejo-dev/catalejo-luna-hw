EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:USB_B_Micro J?
U 1 1 5EACBC62
P 1150 2100
F 0 "J?" H 1207 2567 50  0000 C CNN
F 1 "USB_B_Micro" H 1207 2476 50  0000 C CNN
F 2 "" H 1300 2050 50  0001 C CNN
F 3 "~" H 1300 2050 50  0001 C CNN
	1    1150 2100
	1    0    0    -1  
$EndComp
Text HLabel 1600 2100 2    50   Output ~ 0
D+
Text HLabel 1600 2200 2    50   Output ~ 0
D-
Wire Wire Line
	1450 2100 1600 2100
Wire Wire Line
	1450 2200 1600 2200
NoConn ~ 1450 2300
NoConn ~ 1050 2500
Text Notes 900  1550 0    50   ~ 0
Entrada de energización\nexterna 5V\n\nA futuro se podría considerar\nun conector tipo C si es fácil\nde conseguir en el mercado\nlocal
$Comp
L Device:C C?
U 1 1 5EAD73B0
P 2050 2350
F 0 "C?" H 2165 2396 50  0000 L CNN
F 1 "10uF" H 2165 2305 50  0000 L CNN
F 2 "" H 2088 2200 50  0001 C CNN
F 3 "~" H 2050 2350 50  0001 C CNN
	1    2050 2350
	1    0    0    -1  
$EndComp
Text Notes 1300 2750 0    50   ~ 0
Capacitor protector\ncontra sobre picos\ndebe estar junto al\nusb micro b
Text Notes 2200 3100 0    50   ~ 0
Led indicador de energización\nexterna y elemento que descarga\nel condensador de 10u
$Comp
L Device:LED D?
U 1 1 5EAE220D
P 2550 2550
F 0 "D?" V 2589 2432 50  0000 R CNN
F 1 "RED" V 2498 2432 50  0000 R CNN
F 2 "" H 2550 2550 50  0001 C CNN
F 3 "~" H 2550 2550 50  0001 C CNN
	1    2550 2550
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5EAE1D12
P 2550 2150
F 0 "R?" H 2620 2196 50  0000 L CNN
F 1 "1k" H 2620 2105 50  0000 L CNN
F 2 "" V 2480 2150 50  0001 C CNN
F 3 "~" H 2550 2150 50  0001 C CNN
	1    2550 2150
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 5EAF8EEB
P 4600 3000
F 0 "R?" V 4393 3000 50  0000 C CNN
F 1 "100k" V 4484 3000 50  0000 C CNN
F 2 "" V 4530 3000 50  0001 C CNN
F 3 "~" H 4600 3000 50  0001 C CNN
	1    4600 3000
	-1   0    0    1   
$EndComp
$Comp
L fdt434p:FDT434P Q?
U 1 1 5EAFB19D
P 5100 2350
F 0 "Q?" H 5208 2396 50  0000 L CNN
F 1 "FDT434P" H 5208 2305 50  0000 L CNN
F 2 "SOT230P700X180-4N" H 5100 2350 50  0001 L BNN
F 3 "Fairchild Semiconductor" H 5100 2350 50  0001 L BNN
	1    5100 2350
	1    0    0    -1  
$EndComp
$Comp
L Diode:1N5819 D?
U 1 1 5EAFE3DF
P 4850 2750
F 0 "D?" H 4850 2534 50  0000 C CNN
F 1 "1N5819" H 4850 2625 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 4850 2575 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88525/1n5817.pdf" H 4850 2750 50  0001 C CNN
	1    4850 2750
	-1   0    0    1   
$EndComp
Wire Wire Line
	4700 2750 4600 2750
Wire Wire Line
	4600 2750 4600 2250
Wire Wire Line
	4800 2250 4600 2250
Connection ~ 4600 2250
Wire Wire Line
	5000 2750 5100 2750
Wire Wire Line
	5100 2750 5100 2650
Wire Wire Line
	5100 1950 5100 2050
Text Notes 4250 1550 0    50   ~ 0
Circuito que sirve para seleccionar la fuente de energización\n(externa o batería).\nFuncionamiento: Cuando vusb está desconectado el valor en\nel gate tiende a 0 V por la resistencia de 100k, ésto hace\nque el canal del pmos cierre el circuito entre el vbatt y\nv_sw_power. Cuando el voltaje vbus es 5v el canal del pmos\nabre el circuito entre vbatt y v_sw_power, sin embargo, la\ntensión percibida en el diodo cschotty es en directo,\nentrando éste en conducción y así alimentar a v_sw_power.\nEn ambos casos la tensión em v_sw_power será lo\nsuficientemente grande para que el LDO de 3.3 V entre en\nfuncionamiento.
$Comp
L mcp1826t:MCP1826T-ADJE_DC U?
U 1 1 5EB1CE1D
P 8250 4600
F 0 "U?" H 8275 5015 50  0000 C CNN
F 1 "MCP1826T-ADJE_DC" H 8275 4924 50  0000 C CNN
F 2 "SOT230P700X180-6N" H 8300 4600 50  0001 L BNN
F 3 "Microchip" H 8300 4600 50  0001 L BNN
F 4 "SOT223-5" H 8300 4600 50  0001 L BNN "Field4"
F 5 "MCP1826T-ADJE/DC" H 8300 4600 50  0001 L BNN "Field5"
F 6 "08N6496" H 8300 4600 50  0001 L BNN "Field6"
F 7 "1578432" H 8300 4600 50  0001 L BNN "Field7"
	1    8250 4600
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_DIP_x01 SW?
U 1 1 5EB22369
P 5500 2950
F 0 "SW?" H 5500 3217 50  0000 C CNN
F 1 "switch_on" H 5500 3126 50  0000 C CNN
F 2 "" H 5500 2950 50  0001 C CNN
F 3 "~" H 5500 2950 50  0001 C CNN
	1    5500 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 2950 5900 2950
Text Notes 5500 2600 0    50   ~ 0
Interruptor de encendido y \nde apagado de la placa
$Comp
L Device:C C?
U 1 1 5EB37FC1
P 7050 5000
F 0 "C?" V 7302 5000 50  0000 C CNN
F 1 "10u" V 7211 5000 50  0000 C CNN
F 2 "" H 7088 4850 50  0001 C CNN
F 3 "~" H 7050 5000 50  0001 C CNN
	1    7050 5000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5EB387CE
P 7550 4700
F 0 "R?" V 7343 4700 50  0000 C CNN
F 1 "10k" V 7434 4700 50  0000 C CNN
F 2 "" V 7480 4700 50  0001 C CNN
F 3 "~" H 7550 4700 50  0001 C CNN
	1    7550 4700
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5EB3B726
P 8850 5000
F 0 "C?" H 8965 5046 50  0000 L CNN
F 1 "1u" H 8965 4955 50  0000 L CNN
F 2 "" H 8888 4850 50  0001 C CNN
F 3 "~" H 8850 5000 50  0001 C CNN
	1    8850 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 5150 8850 5250
Wire Wire Line
	8750 4750 8850 4750
Connection ~ 8850 4750
Wire Wire Line
	8850 4750 8850 4850
Wire Wire Line
	7800 4600 7700 4600
Wire Wire Line
	7800 5000 7700 5000
Wire Wire Line
	7700 5000 7700 4900
Wire Wire Line
	7700 4900 7800 4900
Text Notes 7800 5500 0    50   ~ 0
TODO: completar los datos del LDO\nLDO 3V3 a 1 amp
Text Notes 8950 5300 0    50   ~ 0
Reviar el valor\ndel condensador\n!!!
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 5EB5A2BE
P 5300 5350
F 0 "J?" H 5380 5342 50  0000 L CNN
F 1 "Battery" H 5380 5251 50  0000 L CNN
F 2 "" H 5300 5350 50  0001 C CNN
F 3 "~" H 5300 5350 50  0001 C CNN
	1    5300 5350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5EAFBD2D
P 4700 5400
F 0 "C?" H 4815 5446 50  0000 L CNN
F 1 "1u" H 4815 5355 50  0000 L CNN
F 2 "" H 4738 5250 50  0001 C CNN
F 3 "~" H 4700 5400 50  0001 C CNN
	1    4700 5400
	1    0    0    -1  
$EndComp
Text Notes 4800 5750 0    50   ~ 0
Revisar\nvalor de\nC !!!
$Comp
L Switch:SW_DPDT_x2 SW?
U 1 1 5EB0CA90
P 9250 4500
F 0 "SW?" H 9250 4175 50  0000 C CNN
F 1 "SW_VOLT_SELECT" H 9250 4266 50  0000 C CNN
F 2 "" H 9250 4500 50  0001 C CNN
F 3 "~" H 9250 4500 50  0001 C CNN
	1    9250 4500
	-1   0    0    1   
$EndComp
Text Notes 9550 3850 0    50   ~ 0
Definir esquema\ncorrecto para\ninterruptor !!!
Text Notes 4800 3400 0    50   ~ 0
Definir el esquema\ncorrecto para el\nswitch de encendido\nde placa !!!
Text HLabel 9550 4500 2    50   Output ~ 0
VCC
Text Notes 9550 4350 0    50   ~ 0
éste selector\npermite cambiar \nel nivel de tensión\npara los gpio\nrequeridos
Text Label 2050 2900 3    50   ~ 0
GND
Text Label 2050 1800 1    50   ~ 0
VUSB
Text Label 4600 1950 1    50   ~ 0
VUSB
Text Label 4600 3250 3    50   ~ 0
GND
Text Label 5100 1950 1    50   ~ 0
+VBATT
Wire Wire Line
	5100 2750 5100 2950
Wire Wire Line
	5100 2950 5200 2950
Connection ~ 5100 2750
Wire Wire Line
	2550 2000 2550 1900
Wire Wire Line
	2550 2700 2550 2800
Wire Wire Line
	2550 2800 2050 2800
Wire Wire Line
	2050 2800 2050 2500
Wire Wire Line
	2050 2200 2050 1900
Connection ~ 2050 1900
Wire Wire Line
	2050 1900 2550 1900
Wire Wire Line
	2550 2400 2550 2300
Wire Wire Line
	1150 2500 1150 2800
Wire Wire Line
	1150 2800 2050 2800
Connection ~ 2050 2800
Wire Wire Line
	2050 2900 2050 2800
Wire Wire Line
	1450 1900 2050 1900
Wire Wire Line
	2050 1800 2050 1900
Text Label 5900 2850 1    50   ~ 0
VIN
Wire Wire Line
	5900 2850 5900 2950
Connection ~ 7700 5000
Text Label 7050 3900 1    50   ~ 0
VIN
Text Label 8850 3900 1    50   ~ 0
+3.3V
Wire Wire Line
	7700 4400 7800 4400
Wire Wire Line
	7700 4600 7700 4400
Text Label 7700 5350 3    50   ~ 0
GND
Wire Wire Line
	7050 5150 7050 5250
Wire Wire Line
	7050 5250 7700 5250
Wire Wire Line
	7700 5000 7700 5250
Connection ~ 7700 5250
Wire Wire Line
	7700 5350 7700 5250
Wire Wire Line
	7700 5250 8850 5250
$Comp
L tp4056:TP4056 U?
U 1 1 5ECD10D3
P 3300 5350
F 0 "U?" H 3275 5865 50  0000 C CNN
F 1 "TP4056" H 3275 5774 50  0000 C CNN
F 2 "SOP-8" H 3300 5350 50  0001 L BNN
F 3 "" H 3300 5350 50  0001 C CNN
	1    3300 5350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5ECD1123
P 2300 5700
F 0 "R?" H 2370 5746 50  0000 L CNN
F 1 "1k" H 2370 5655 50  0000 L CNN
F 2 "" V 2230 5700 50  0001 C CNN
F 3 "~" H 2300 5700 50  0001 C CNN
	1    2300 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 5150 2700 5150
Wire Wire Line
	2700 5150 2700 5050
Wire Wire Line
	2700 5050 2800 5050
Connection ~ 2700 5050
$Comp
L Device:R R?
U 1 1 5ECD1147
P 3850 5700
F 0 "R?" H 3920 5746 50  0000 L CNN
F 1 "1.2k" H 3920 5655 50  0000 L CNN
F 2 "" V 3780 5700 50  0001 C CNN
F 3 "~" H 3850 5700 50  0001 C CNN
	1    3850 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 5550 3850 5450
Wire Wire Line
	3850 5450 3750 5450
Wire Wire Line
	3850 5850 3850 5950
Text Notes 2850 6250 0    50   ~ 0
Ésta resistencia\nse encarga de\ndefinir la velocidad\nde carga para la\nbatería, se requiere\nrevisar detenidamente\nel datasheet !!!
Text Notes 2400 4700 0    50   ~ 0
Se debe buscar en el datasheet\ny especificar en éstas líneas\nel funcionamiento y los indicadores\ndel cargador !!!
$Comp
L Device:R R?
U 1 1 5ECF4DF2
P 1850 5700
F 0 "R?" H 1920 5746 50  0000 L CNN
F 1 "1k" H 1920 5655 50  0000 L CNN
F 2 "" V 1780 5700 50  0001 C CNN
F 3 "~" H 1850 5700 50  0001 C CNN
	1    1850 5700
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 5ECD112D
P 2300 5300
F 0 "D?" V 2339 5182 50  0000 R CNN
F 1 "RED" V 2248 5182 50  0000 R CNN
F 2 "" H 2300 5300 50  0001 C CNN
F 3 "~" H 2300 5300 50  0001 C CNN
	1    2300 5300
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D?
U 1 1 5ECD1137
P 1850 5300
F 0 "D?" V 1889 5182 50  0000 R CNN
F 1 "GREEN" V 1798 5182 50  0000 R CNN
F 2 "" H 1850 5300 50  0001 C CNN
F 3 "~" H 1850 5300 50  0001 C CNN
	1    1850 5300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2300 5050 2300 5150
Connection ~ 2300 5050
Wire Wire Line
	2300 5050 2700 5050
Wire Wire Line
	2300 5450 2300 5550
Wire Wire Line
	2800 5350 2600 5350
Wire Wire Line
	2600 5350 2600 5950
Wire Wire Line
	2600 5950 2300 5950
Wire Wire Line
	2300 5950 2300 5850
Wire Wire Line
	1850 5450 1850 5550
Wire Wire Line
	1850 5150 1850 5050
Wire Wire Line
	1850 5050 2300 5050
Text Label 1850 4950 1    50   ~ 0
VUSB
Wire Wire Line
	1850 4950 1850 5050
Connection ~ 1850 5050
Wire Wire Line
	1850 6050 2700 6050
Wire Wire Line
	2700 6050 2700 5450
Wire Wire Line
	2700 5450 2800 5450
Wire Wire Line
	1850 5850 1850 6050
Text Label 3850 6050 3    50   ~ 0
GND
Text Label 4700 4950 1    50   ~ 0
+VBATT
Wire Wire Line
	4150 5350 4150 5950
Wire Wire Line
	4150 5950 3850 5950
Wire Wire Line
	3750 5350 4150 5350
Wire Wire Line
	3850 5950 3300 5950
Wire Wire Line
	3300 5950 3300 5700
Connection ~ 3850 5950
Wire Wire Line
	3850 5950 3850 6050
Wire Wire Line
	4600 3150 4600 3250
Wire Wire Line
	4600 2850 4600 2750
Connection ~ 4600 2750
Wire Wire Line
	5100 5450 5000 5450
Wire Wire Line
	5000 5450 5000 5950
Wire Wire Line
	5000 5950 4700 5950
Connection ~ 4150 5950
Wire Wire Line
	4700 5550 4700 5950
Connection ~ 4700 5950
Wire Wire Line
	4700 5950 4150 5950
Wire Wire Line
	5000 5050 5000 5350
Wire Wire Line
	5000 5350 5100 5350
Wire Wire Line
	4700 5250 4700 5050
Connection ~ 4700 5050
Wire Wire Line
	4700 5050 5000 5050
Wire Wire Line
	3750 5050 4700 5050
Wire Wire Line
	4700 4950 4700 5050
Text HLabel 9550 4950 2    50   Output ~ 0
GND
Text HLabel 9550 4750 2    50   Output ~ 0
+3.3V
Wire Wire Line
	8850 4750 8950 4750
Wire Wire Line
	8850 4100 7300 4100
Wire Wire Line
	8850 4100 8850 4750
Wire Wire Line
	7800 4700 7700 4700
Wire Wire Line
	7400 4700 7300 4700
Wire Wire Line
	7300 4700 7300 4100
Wire Wire Line
	9050 4400 8950 4400
Wire Wire Line
	8950 4400 8950 4000
Wire Wire Line
	8950 4000 7700 4000
Wire Wire Line
	8850 3900 8850 4100
Connection ~ 8850 4100
Wire Wire Line
	7050 3900 7050 4000
Connection ~ 7050 4000
Wire Wire Line
	9050 4600 8950 4600
Wire Wire Line
	8950 4600 8950 4750
Wire Wire Line
	9550 4500 9450 4500
Wire Wire Line
	9550 4750 8950 4750
Connection ~ 8950 4750
Wire Wire Line
	9550 4950 9450 4950
Wire Wire Line
	9450 4950 9450 5250
Wire Wire Line
	9450 5250 8850 5250
Connection ~ 8850 5250
Wire Wire Line
	7050 4000 7050 4850
Wire Wire Line
	7700 4400 7700 4000
Connection ~ 7700 4400
Connection ~ 7700 4000
Wire Wire Line
	7700 4000 7050 4000
Wire Wire Line
	4600 1950 4600 2250
$EndSCHEMATC

EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LED:WS2812B D?
U 1 1 5EEF0B65
P 5000 3550
F 0 "D?" H 5344 3596 50  0000 L CNN
F 1 "WS2812B" H 5344 3505 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 5050 3250 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 5100 3175 50  0001 L TNN
	1    5000 3550
	1    0    0    -1  
$EndComp
NoConn ~ 5300 3550
$Comp
L Device:R R?
U 1 1 5EEF1670
P 4450 3550
F 0 "R?" V 4243 3550 50  0000 C CNN
F 1 "330" V 4334 3550 50  0000 C CNN
F 2 "" V 4380 3550 50  0001 C CNN
F 3 "~" H 4450 3550 50  0001 C CNN
	1    4450 3550
	0    1    1    0   
$EndComp
Text Label 5000 3050 1    50   ~ 0
+3.3V
Text Label 5000 4050 3    50   ~ 0
GND
Wire Wire Line
	4200 3550 4300 3550
Wire Wire Line
	5000 3850 5000 3950
Wire Wire Line
	4700 3550 4600 3550
Wire Wire Line
	5000 3050 5000 3150
Text HLabel 4200 3550 0    50   Input ~ 0
DIN_SERIAL
Text HLabel 4200 3150 0    50   Input ~ 0
+3.3V
Wire Wire Line
	4200 3150 5000 3150
Connection ~ 5000 3150
Wire Wire Line
	5000 3150 5000 3250
Text HLabel 4200 3950 0    50   Input ~ 0
GND
Wire Wire Line
	4200 3950 5000 3950
Connection ~ 5000 3950
Wire Wire Line
	5000 3950 5000 4050
$EndSCHEMATC

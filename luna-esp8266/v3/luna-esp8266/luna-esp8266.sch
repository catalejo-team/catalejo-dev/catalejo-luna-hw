EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1850 2550 650  1000
U 5EB234FE
F0 "power" 50
F1 "power.sch" 50
F2 "D+" O R 2500 2700 50 
F3 "D-" O R 2500 2850 50 
F4 "GND" O R 2500 3400 50 
F5 "+3.3V" O R 2500 3250 50 
F6 "+5v" O R 2500 3100 50 
$EndSheet
$Sheet
S 6300 1400 550  500 
U 5EB295E9
F0 "audio" 50
F1 "audio.sch" 50
F2 "IN" I L 6300 1500 50 
F3 "GND" I L 6300 1800 50 
F4 "VCC" I L 6300 1650 50 
$EndSheet
$Sheet
S 6300 2200 1000 500 
U 5EB2A2A4
F0 "led_rgb_ws2812b" 50
F1 "led_rgb_ws2812b.sch" 50
F2 "DIN_SERIAL" I L 6300 2300 50 
F3 "GND" I L 6300 2600 50 
F4 "+5V" I L 6300 2450 50 
$EndSheet
$Sheet
S 6300 3000 1350 2850
U 5EB28A09
F0 "gpio-luna-mini-interface" 50
F1 "gpio-luna-mini-interface.sch" 50
F2 "JA2" B L 6300 3100 50 
F3 "JA1" B L 6300 3200 50 
F4 "JA0" B L 6300 3300 50 
F5 "JA3" B L 6300 3400 50 
F6 "VCC" I L 6300 5600 50 
F7 "GND" I L 6300 5750 50 
F8 "JB2" B L 6300 3550 50 
F9 "JB1" B L 6300 3650 50 
F10 "JB0" B L 6300 3750 50 
F11 "JB3" B L 6300 3850 50 
F12 "JC2" B L 6300 4000 50 
F13 "JC1" B L 6300 4100 50 
F14 "JC0" B L 6300 4200 50 
F15 "JC3" B L 6300 4300 50 
F16 "JD2" B L 6300 4450 50 
F17 "JD1" B L 6300 4550 50 
F18 "JD0" B L 6300 4650 50 
F19 "JD3" B L 6300 4750 50 
F20 "JE2" B L 6300 4900 50 
F21 "JE1" B L 6300 5000 50 
F22 "JE0" B L 6300 5100 50 
F23 "JE3" B L 6300 5200 50 
F24 "SCL" I L 6300 5450 50 
F25 "SDA" B L 6300 5350 50 
$EndSheet
Text Notes 2950 5550 0    50   ~ 0
Pin -> Observación\n0 [16] -> compartido, wake up, no pwm, gpio\n1 [14] -> libre, pwm, gpio\n2 [12] -> libre, pwm, gpio\n3 [RX] -> compartido, RX, pwm, gpio\n4 [TX] -> compartido, TX, pwm, gpio\n5 [2] -> compartido: WS2812B, pull up, pwm\n6 [13] -> libre, pwm, gpio\n7 [15] -> exclusivo: PARLANTE, pull down para iniciar\n8 [0] -> exclusivo: BTN USER, pull up para iniciar\n9 [5] -> exclusivo: SDA por SW\n10[4]-> exclusivo: SCL por SW, sirve para pwm, gpio
Text Notes 7800 4400 0    50   ~ 0
SPI\n______________\nJ2 -> SCLK\nJ1 -> MISO\nJ0 -> MOSI\nVCC\nGND\nJ3 -> CS\n
Text Notes 8350 4850 0    50   ~ 0
I2C\n_____________\nJ2 -> X\nJ1 -> SDA\nJ0 -> SCL\nVCC\nGND\nJ3 -> X\n
Text Notes 8350 4000 0    50   ~ 0
UART\n_____________\nJ2 -> X\nJ1 -> RX\nJ0 -> TX\nVCC\nGND\nJ3 -> X\n
Text Notes 7800 3500 0    50   ~ 0
ADC\n_____________\nJ2 -> A1\nJ1 -> D0\nJ0 -> A0\nVCC\nGND\nJ3 -> D1\n
Text Notes 7800 5300 0    50   ~ 0
I2C\n_____________\nJ2 -> X\nJ1 -> SDA\nJ0 -> SCL\nVCC\nGND\nJ3 -> X\n
NoConn ~ 6300 3100
NoConn ~ 6300 3400
NoConn ~ 6300 3550
NoConn ~ 6300 3850
NoConn ~ 6300 4450
NoConn ~ 6300 4750
NoConn ~ 6300 4900
NoConn ~ 6300 5200
Wire Wire Line
	6300 4550 6100 4550
Wire Wire Line
	6100 4550 6100 5000
Wire Wire Line
	6100 5000 6300 5000
Wire Wire Line
	6300 5350 6100 5350
Wire Wire Line
	6100 5350 6100 5000
Connection ~ 6100 5000
Wire Wire Line
	6300 4650 6200 4650
Wire Wire Line
	6200 4650 6200 5100
Wire Wire Line
	6200 5100 6300 5100
Wire Wire Line
	6300 5450 6200 5450
Wire Wire Line
	6200 5450 6200 5100
Connection ~ 6200 5100
$Sheet
S 3450 3150 850  1300
U 5EB2CD91
F0 "esp8266-12" 50
F1 "esp8266-12.sch" 50
F2 "A0" I R 4300 3300 50 
F3 "14" B R 4300 3850 50 
F4 "12" B R 4300 3950 50 
F5 "RX" B R 4300 3650 50 
F6 "TX" B R 4300 3750 50 
F7 "2" B R 4300 3200 50 
F8 "13" B R 4300 4050 50 
F9 "15" B R 4300 4150 50 
F10 "5" B R 4300 4250 50 
F11 "4" B R 4300 4350 50 
F12 "+3.3V" I L 3450 3250 50 
F13 "GND" I L 3450 3400 50 
$EndSheet
NoConn ~ 2500 2700
NoConn ~ 2500 2850
Text Label 6150 3650 2    50   ~ 0
RX
Text Label 6150 3750 2    50   ~ 0
TX
Text Label 6200 3300 2    50   ~ 0
A0
Wire Wire Line
	2500 3400 2850 3400
Wire Wire Line
	2500 3100 2700 3100
Connection ~ 2850 3400
Wire Wire Line
	2850 3400 3150 3400
Wire Wire Line
	6300 1800 2850 1800
Wire Wire Line
	6300 2600 3150 2600
Wire Wire Line
	3150 2600 3150 3400
Connection ~ 3150 3400
Wire Wire Line
	3150 3400 3450 3400
Wire Wire Line
	2700 3100 2700 5600
Wire Wire Line
	2700 5600 6300 5600
Connection ~ 2700 3100
Wire Wire Line
	2850 3400 2850 5750
Wire Wire Line
	2850 5750 6300 5750
Text Label 4500 4350 2    50   ~ 0
SCL
Text Label 4500 4250 2    50   ~ 0
SDA
Text Label 6150 4300 2    50   ~ 0
CS
Text Label 6150 4200 2    50   ~ 0
MOSI
Text Label 6200 3200 2    50   ~ 0
WS
Wire Wire Line
	6200 5450 5150 5450
Wire Wire Line
	5150 5450 5150 4350
Wire Wire Line
	4300 4350 5150 4350
Connection ~ 6200 5450
Wire Wire Line
	6100 5350 5300 5350
Wire Wire Line
	5300 5350 5300 4250
Wire Wire Line
	4300 4250 5300 4250
Connection ~ 6100 5350
Text Notes 5850 4700 0    50   ~ 0
I2C A
Text Notes 5850 5050 0    50   ~ 0
I2C B
Wire Wire Line
	4300 3750 6300 3750
Wire Wire Line
	4300 3650 6300 3650
Wire Wire Line
	2850 1800 2850 3400
Wire Wire Line
	4300 3300 6300 3300
Wire Wire Line
	4300 3200 5400 3200
Wire Wire Line
	5450 4300 5450 4150
Wire Wire Line
	5450 4300 6300 4300
Wire Wire Line
	5600 4200 5600 4050
Wire Wire Line
	5600 4200 6300 4200
Wire Wire Line
	6300 4100 5750 4100
Wire Wire Line
	5750 4100 5750 3950
Wire Wire Line
	4300 3950 5750 3950
Wire Wire Line
	6300 4000 5900 4000
Wire Wire Line
	5900 4000 5900 3850
Wire Wire Line
	4300 3850 5900 3850
Wire Wire Line
	5250 1500 6300 1500
Wire Wire Line
	6300 2300 5400 2300
Wire Wire Line
	5400 2300 5400 3200
Connection ~ 5400 3200
Wire Wire Line
	5400 3200 6300 3200
Text Label 6150 4100 2    50   ~ 0
MISO
Text Notes 6000 3900 0    50   ~ 0
SPI
Text Label 6150 4000 2    50   ~ 0
SCLK
Wire Wire Line
	4300 4050 5600 4050
Wire Wire Line
	5250 1500 5250 4150
Wire Wire Line
	4300 4150 5250 4150
Connection ~ 5250 4150
Wire Wire Line
	5250 4150 5450 4150
Wire Wire Line
	2500 3250 3450 3250
Wire Wire Line
	2700 1650 2700 3100
Wire Wire Line
	2700 1650 3150 1650
Wire Wire Line
	6300 2450 3150 2450
Wire Wire Line
	3150 2450 3150 1650
Connection ~ 3150 1650
Wire Wire Line
	3150 1650 6300 1650
$Comp
L Connector_Generic:Conn_01x01 Js1
U 1 1 5EB7FF75
P 4750 6300
F 0 "Js1" V 4622 6380 50  0000 L CNN
F 1 "hole" V 4713 6380 50  0000 L CNN
F 2 "TestPoint:TestPoint_Plated_Hole_D2.0mm" H 4750 6300 50  0001 C CNN
F 3 "~" H 4750 6300 50  0001 C CNN
	1    4750 6300
	0    1    1    0   
$EndComp
NoConn ~ 4750 6100
$Comp
L Connector_Generic:Conn_01x01 Js2
U 1 1 5EB843F7
P 5150 6300
F 0 "Js2" V 5022 6380 50  0000 L CNN
F 1 "hole" V 5113 6380 50  0000 L CNN
F 2 "TestPoint:TestPoint_Plated_Hole_D2.0mm" H 5150 6300 50  0001 C CNN
F 3 "~" H 5150 6300 50  0001 C CNN
	1    5150 6300
	0    1    1    0   
$EndComp
NoConn ~ 5150 6100
$EndSCHEMATC

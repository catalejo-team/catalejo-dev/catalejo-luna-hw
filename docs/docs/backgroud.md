# Conectores

## RJ Registered jack

Estandar para interfaces físicas de computadores.

Características:

```bash
XCYP
||||__ P de posiciones
|||___ # de posiciones
||____ C de Contactos implica la misma cantidad de cuchillas para crimpar
|_____ # de Contactos
```

Ejmplo:

* `6P6C`: El conector tiene 6 conectores y 6 posiciones, los conectores implican la cantidad de cuchillas para crimpar.


| Type  | Features | Observations    |
|:-----:|:--------:|:---------------:|
| RJ-9  | 4P4C     | No oficial name |
| RJ-10 | 4P4C
| RJ-11 | 6P4C
| RJ-12 |
| RJ-14
| RJ-21
| RJ-45
| RJ-48
| RJ-50
| RJ-61

## Crimpadora

Instrumento para ponchar "crimpar".

# Card Slot

* Connector Game Cartridge Card Slot for SEGA
* [GBA connector](https://www.ebay.com/itm/NEW-Cartridge-Slot-Connector-Replacement-Nintendo-Game-Boy-Advance-GBA-SP-/292742358765)
* [Edge Connector](https://en.wikipedia.org/wiki/Edge_connector)
* sockets
